'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
exports.linkTypeDefs = `
    type AdminCreateClaimPayload {
        code: String
        success: Boolean
        message: String
        payload: AdminClaimPayload
    }
    input AdminCreateClaimInput {
        rentalId: ID!
    }
    extend type Mutation {
        adminCreateClaim(data: AdminCreateClaimInput!): AdminCreateClaimPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    adminCreateClaim(_, args, ctx, info) {
      return __awaiter(this, void 0, void 0, function*() {
        const { rentalId } = args.data
        try {
          const user = yield ctx.users.db.query.user({
            where: {
              id: ctx.currentUser.id,
            },
          })
          const insurance = yield ctx.insurance.db.query.insurances({
            first: 1,
            orderBy: `startDateTime_DESC`,
            where: {
              rentalId: rentalId,
            },
          })
          const rental = yield ctx.rentals.db.query.rental({
            where: {
              id: rentalId,
            },
          })
          const car = yield ctx.cars.db.query.car({
            where: {
              id: rental.carId,
            },
          })
          const payload = yield ctx.admin.mutation.adminReportClaim(
            {
              data: {
                rentalId,
                carId: car.id,
                insuranceId:
                  lib_1.optionalChaining(() => insurance[0].id) || null,
                ownerId: car.ownerId,
                driverId: rental.driverId,
                status: `REPORTED`,
                submittedBy: {
                  create: {
                    userId: ctx.currentUser.id,
                    submittedVia: 'ADMIN',
                  },
                },
              },
              by: {
                id: user.id,
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
              },
            },
            ctx,
            `{
            claim{
              id
              submittedBy{
                id
                userId
              }
            }
            by{
              id
              firstName
              lastName
              email
            }
          }`,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            payload,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.REQUEST_FAILED,
            success: false,
            message: e.message,
          }
        }
      })
    },
  },
})
