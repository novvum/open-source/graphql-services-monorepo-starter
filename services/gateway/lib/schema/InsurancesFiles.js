'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type Insurance {
      insuranceCardImage: File
  }
  extend type PreInsurancePhoto {
    file: File
  }
`
exports.resolvers = mergeInfo => ({
  Insurance: {
    insuranceCardImage: {
      fragment: `fragment InsuranceCardImageFragment on Insurance { id }`,
      resolve(parent, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          if (!parent.id) {
            return null
          }
          const insurance = yield context.insurance.query.insurance(
            {
              where: {
                id: parent.id,
              },
            },
            context,
            '{ id insuranceCardImageId }',
          )
          return context.files.query.file(
            {
              where: {
                id: insurance.insuranceCardImageId,
              },
            },
            context,
            info,
          )
        })
      },
    },
  },
  PreInsurancePhoto: {
    file: {
      fragment: `fragment FilePreInsurancePhotoFragment on PreInsurancePhoto { id }`,
      resolve(parent, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const preInsurancePhoto = yield context.insurance.query.preInsurancePhoto(
            {
              where: {
                id: parent.id,
              },
            },
            context,
            '{ id fileId }',
          )
          const id = preInsurancePhoto.fileId
          if (!id) {
            return null
          }
          return yield context.files.query.file(
            {
              where: {
                id: id,
              },
            },
            context,
            info,
          )
        })
      },
    },
  },
})
