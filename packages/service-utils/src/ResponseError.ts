export const makeResponseError = (args: { message: string; code: any }) => {
  const e = new Error(args.message)
  // @ts-ignore
  e.code = args.code
  return e
}
