import { MergedContext } from '../../getContext'

export const linkTypeDefs = `
    extend type Mutation {
        sampleMutation(id: ID!): Boolean
    }
    extend type Query {
        getSampleQuery: Boolean
    }
`

export const resolvers: any = {
  Query: {
    async getSampleQuery(_, args, context: MergedContext, info) {
      return true
    },
  },
  Mutation: {
    async sampleMutation(_, args, context: MergedContext, info) {
      return true
    },
  },
}
