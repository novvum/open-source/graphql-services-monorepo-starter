'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type Viewer {
    cars(where: CarWhereInput, orderBy: CarOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): CarConnection!
  }
  extend type Car {
      owner: Owner
  }
  extend type Owner {
    cars(where: CarWhereInput, orderBy: CarOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): CarConnection!
}
`
exports.resolvers = mergeInfo => ({
  Car: {
    owner: {
      fragment: `fragment OwnerFragment on Car { ownerId }`,
      resolve(parent, _, context, info) {
        const ownerId = parent.ownerId
        return mergeInfo.delegate(
          'query',
          'owner',
          {
            where: {
              id: ownerId,
            },
          },
          context,
          info,
        )
      },
    },
  },
  Owner: {
    cars: {
      fragment: `fragment CarFragment on Owner { id }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.id
          if (!id) {
            return null
          }
          // TODO: get list of owner's car id's for OR condition below
          return mergeInfo.delegate(
            'query',
            'carsConnection',
            Object.assign({}, args, {
              where: {
                AND: [{ ownerId: id }, args.where || {}],
              },
            }),
            context,
            info,
          )
        })
      },
    },
  },
  Viewer: {
    cars: {
      fragment: `fragment ViewerFragment on Viewer { me { owner { id } } }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id =
            parent.me &&
            parent.me.owner &&
            parent.me.owner &&
            parent.me.owner.id
          if (!id) {
            return null
          }
          // TODO: get list of owner's car id's for OR condition below
          return mergeInfo.delegate(
            'query',
            'carsConnection',
            Object.assign({}, args, {
              where: {
                AND: [
                  {
                    ownerId: id,
                  },
                  args.where || {},
                ],
              },
            }),
            context,
            info,
          )
        })
      },
    },
  },
})
