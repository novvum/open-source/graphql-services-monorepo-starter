'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
exports.linkTypeDefs = `
    extend type Query {
        getCarPriceBreakdown(
            carId: ID!
            rentalDays: Int!
            couponCode: String
        ): CarPriceBreakdown
    }
`
exports.resolvers = mergeInfo => ({
  Query: {
    getCarPriceBreakdown: (_, args, context, info) =>
      __awaiter(this, void 0, void 0, function*() {
        const carDetails = yield context.cars.query.car(
          {
            where: {
              id: args.carId,
            },
          },
          context,
          `{id dailyPriceInCents weeklyPriceInCents monthlyPriceInCents}`,
        )
        if (!carDetails) {
          return null
        }
        const carId = carDetails.id
        const driverUserId = context.currentUser ? context.currentUser.id : null
        const showDeposit = yield checkDepositRequired(
          { driverUserId, carId },
          context,
        )
        return context.payments.query.getPriceBreakdown(
          {
            where: {
              rentalDays: args.rentalDays,
              dailyPriceInCents: carDetails.dailyPriceInCents,
              weeklyPriceInCents: carDetails.weeklyPriceInCents,
              monthlyPriceInCents: carDetails.monthlyPriceInCents,
              showDeposit,
              couponCode: args.couponCode,
            },
          },
          context,
          info,
        )
      }),
  },
})
function checkDepositRequired(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    if (!args.driverUserId) {
      return false
    }
    const [car, paymentAccounts] = yield Promise.all([
      context.cars.query.car(
        {
          where: {
            id: args.carId,
          },
        },
        context,
        '{ id location { id state } }',
      ),
      context.payments.query.paymentAccounts(
        {
          where: {
            userId: args.driverUserId,
          },
          first: 1,
        },
        context,
        `{
            id
            stripePaymentInformation {
                id
                customerId
                source {
                    id
                    funding
                }
            }
        }`,
      ),
    ])
    if (
      !car.location ||
      !lib_1.optionalChaining(
        () => paymentAccounts[0].stripePaymentInformation.customerId,
      )
    ) {
      return false
    }
    const paymentAccount = paymentAccounts[0]
    const fundingType = lib_1.optionalChaining(
      () => paymentAccount.stripePaymentInformation.source.funding,
    )
    const carState = car.location.state
    return checkShowDeposit({ fundingType, carState })
  })
}
exports.checkDepositRequired = checkDepositRequired
const EXPERIMENTAL_STATES = ['NV', 'FL', 'CO']
function checkShowDeposit({ fundingType, carState }) {
  if (EXPERIMENTAL_STATES.indexOf(carState) >= 0) {
    return false
  }
  let showDeposit = false
  if (fundingType === 'debit') {
    showDeposit = true
  }
  return showDeposit
}
