import { makeExecutableSchema } from 'graphql-tools'
import { importSchema } from 'graphql-import'
var path = require('path')
let typeDefs = `
  type Query {
    _: Boolean
  }
  type Mutation {
    _: Boolean
  }
`
if (
  path.join(__dirname, './generated/app.graphql') !== '/generated/app.graphql'
) {
  typeDefs = importSchema(path.join(__dirname, './generated/app.graphql'))
}
const schema: any = makeExecutableSchema({
  typeDefs: typeDefs,
  resolverValidationOptions: {
    requireResolversForResolveType: false,
  },
})

export default schema
