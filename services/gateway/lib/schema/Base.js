'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const graphql_tools_1 = require('graphql-tools')
exports.baseSchema = graphql_tools_1.makeExecutableSchema({
  typeDefs: `
		interface MutationResponse {
			code: String
			success: Boolean
			message: String
		}
		enum OffersSortByEnum {
			RELEVANCE
			PRICE_ASC
			PRICE_DESC
			DISTANCE
		}
		type GeoState {
			id: ID
			code: String
			state: String
		}
		type Query {
		    _: Boolean
		}
	`,
})
