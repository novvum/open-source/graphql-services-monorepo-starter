import * as express from 'express'

export type Request = express.Request
export type Response = express.Response

export type ServicesContext = {
  request: Request
  [key: string]: any
}
