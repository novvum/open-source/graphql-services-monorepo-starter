/**
200 - OK Everything worked as expected.

400 - Bad Request The request was unacceptable, often due to missing a required parameter.

401 - Unauthorized No valid API key provided.

402 - Request Failed The parameters were valid but the request failed.

404 - Not Found The requested resource doesn't exist.

409 - Conflict The request conflicts with another request (perhaps due to using the same idempotent key).

429 - Too Many Requests Too many requests hit the API too quickly. We recommend an exponential backoff of your requests.

500, 502, 503, 504 - Server Errors Something went wrong on HyreCar's end. (These should be rare.)

 */
export const ResponseCodes = {
  OK: 200,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  REQUEST_FAILED: 402,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  CONFLICT: 409,
  TOO_MANY_REQUESTS: 429,
  serverError: {
    GENERIC: 500,
    BAD_GATEWAY: 502,
    SERVICE_NOT_AVAILABLE: 503,
    TIMEOUT: 504,
  },
}

export const NOT_AUTHORIZED_RESPONSE = {
  code: ResponseCodes.BAD_REQUEST,
  success: false,
  message: 'You are not authorized to perform this operation.',
  rental: null,
}

export const CARSWITCH_UNAVAILABLE = {
  code: ResponseCodes.CONFLICT,
  success: false,
  message: 'The car you are trying to switch your rental to is unavailable',
  rental: null,
}

export const CARSWITCH_UNVERIFIED = {
  code: ResponseCodes.CONFLICT,
  success: false,
  message:
    'The car you are trying to switch your rental to has not been verified by HyreCar yet',
  rental: null,
}

export const CARSWITCH_LATE_RENTAL = {
  code: ResponseCodes.CONFLICT,
  success: false,
  message:
    'Request Denied. The current rental is late and cannot be switched to a different car',
  rental: null,
}
