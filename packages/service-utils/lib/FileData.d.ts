export declare type FileData = {
  stream: any
  filename: string
  mimetype: string
  encoding: string
}
