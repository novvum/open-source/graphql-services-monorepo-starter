'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const approveRejectBooking_1 = require('./approveRejectBooking')
const utils_1 = require('./utils')
const extendRental_1 = require('./extendRental')
exports.linkTypeDefs = `
    input ConfirmDropoffInput {
        rentalId: ID!
        isDamaged: Boolean!
        surveyComment: String
    }
    type ConfirmDropoffPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        rental: Rental
	}
	input AdminConfirmDropoffInput {
        droppedOffAt: DateTime!
    }
    extend type Mutation {
        confirmDropoff(data: ConfirmDropoffInput!): ConfirmDropoffPayload
        adminConfirmDropoff(data: ConfirmDropoffInput!, adminData: AdminConfirmDropoffInput!): ConfirmDropoffPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    confirmDropoff(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const input = args.data
          yield approveRejectBooking_1.validateRentalOwnerRequest(
            input.rentalId,
            context,
          )
          const updatedRental = yield confirmDropoff({ input }, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            rental: updatedRental,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            rental: null,
          }
        }
      })
    },
    adminConfirmDropoff(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const input = args.data
          const updatedRental = yield confirmDropoff(
            { input, adminInput: args.adminData },
            context,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            rental: updatedRental,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            rental: null,
          }
        }
      })
    },
  },
})
function confirmDropoff(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { input, adminInput } = args
    const rental = yield getRequiredData(input, context)
    const updatedInsurance = yield updateInsurance({ input, rental }, context)
    const updatedRental = yield dropoffRental(
      {
        rental,
        droppedOffAt: lib_1.optionalChaining(() => adminInput.droppedOffAt),
      },
      context,
    )
    yield context.cars.mutation.addCarToMarket(
      {
        data: {
          carId: rental.carId,
        },
      },
      context,
      '{ id }',
    )
    return updatedRental
  })
}
exports.confirmDropoff = confirmDropoff
function validateRequest(rental, context) {
  return __awaiter(this, void 0, void 0, function*() {
    if (
      !(yield approveRejectBooking_1.isCurrentUserCarOwner(
        rental.carId,
        context,
      ))
    ) {
      throw lib_1.makeResponseError({
        code: lib_1.NOT_AUTHORIZED_RESPONSE.code,
        message: lib_1.NOT_AUTHORIZED_RESPONSE.message,
      })
    }
  })
}
function getRequiredData(input, context) {
  return __awaiter(this, void 0, void 0, function*() {
    return yield context.rentals.query.rental(
      {
        where: {
          id: input.rentalId,
        },
      },
      context,
      '{ id driverId disallowExtensions carId rentalPeriods { id } }',
    )
  })
}
exports.getRequiredData = getRequiredData
function dropoffRental(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { rental, droppedOffAt } = args
    const updatedRental = yield context.rentals.mutation.updateRental(
      {
        where: {
          id: rental.id,
        },
        data: {
          status: 'COMPLETED',
          droppedOffAt: droppedOffAt || lib_1.getDate(new Date()),
        },
      },
      context,
      '{ id droppedOffAt status}',
    )
    extendRental_1.updateDriverNotificationStatus(
      updatedRental.id,
      updatedRental.status,
    )
    return updatedRental
  })
}
function updateInsurance(args, ctx) {
  return __awaiter(this, void 0, void 0, function*() {
    const { input } = args
    const insurance = yield utils_1.getCurrentRentalInsurance(args.rental, ctx)
    if (!insurance) {
      return
    }
    try {
      const closedContract = yield ctx.insurance.mutation.closeInsurance(
        {
          insuranceId: insurance.id,
          isDamaged: input.isDamaged,
        },
        ctx,
      )
      return closedContract
    } catch (error) {
      console.error(error)
      throw lib_1.makeResponseError({
        message: error.message,
        code: lib_1.ResponseCodes.BAD_REQUEST,
      })
    }
  })
}
