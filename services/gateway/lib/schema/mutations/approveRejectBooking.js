'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const apollo_fetch_1 = require('apollo-fetch')
const uri = 'https://api.graph.cool/simple/v1/cjgefci8x19990196lpbbhc2h'
const apolloFetch = apollo_fetch_1.createApolloFetch({ uri })
exports.linkTypeDefs = `
    input ApproveRejectBookingInput {
        rentalId: ID!
    }
    type ApproveRejectBookingPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        rental: Rental
    }
    extend type Mutation {
        """Rental's car must be owned by current user, specified rental must be in APPLIED status approves specified rental and auto-rejects other rentals that are in APPLIED/APPLIED_NOT_VERIFIED statuses"""
        approveBooking(data: ApproveRejectBookingInput!): ApproveRejectBookingPayload
        adminApproveBooking(data: ApproveRejectBookingInput!): ApproveRejectBookingPayload
        """Rental's car must be owned by current user, specified rental must be in APPLIED status"""
        rejectBooking(data: ApproveRejectBookingInput!): ApproveRejectBookingPayload
        adminRejectBooking(data: ApproveRejectBookingInput!): ApproveRejectBookingPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    approveBooking(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          yield validateRentalOwnerRequest(args.data.rentalId, context)
          const otherDriverRentals = yield getOtherDriverRentals(
            { rentalId: args.data.rentalId },
            context,
          )
          const approvedRental = yield approveRental(
            args.data.rentalId,
            context,
          )
          notifyRejectedOwners(otherDriverRentals, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
            rental: approvedRental,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            rental: null,
          }
        }
      })
    },
    adminApproveBooking(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const approvedRental = yield approveRental(
            args.data.rentalId,
            context,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
            rental: approvedRental,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            rental: null,
          }
        }
      })
    },
    rejectBooking(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          yield validateRentalOwnerRequest(args.data.rentalId, context)
          const rejectedRental = yield rejectRental(args.data.rentalId, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
            rental: rejectedRental,
          }
        } catch (e) {
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            rental: null,
          }
        }
      })
    },
    adminRejectBooking(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const rejectedRental = yield rejectRental(args.data.rentalId, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
            rental: rejectedRental,
          }
        } catch (e) {
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            rental: null,
          }
        }
      })
    },
  },
})
function validateRentalOwnerRequest(rentalId, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const rental = yield getRental(rentalId, context)
    if (!(yield isCurrentUserCarOwner(rental.carId, context))) {
      throw lib_1.makeResponseError({
        message: lib_1.NOT_AUTHORIZED_RESPONSE.message,
        code: lib_1.NOT_AUTHORIZED_RESPONSE.code,
      })
    }
  })
}
exports.validateRentalOwnerRequest = validateRentalOwnerRequest
function rejectRental(rentalId, context) {
  return __awaiter(this, void 0, void 0, function*() {
    return yield context.rentals.mutation.rejectRental(
      {
        data: {
          rentalId: rentalId,
        },
      },
      context,
      `{
        id
        status
    }`,
    )
  })
}
function getRental(rentalId, context) {
  return __awaiter(this, void 0, void 0, function*() {
    return yield context.rentals.query.rental(
      {
        where: {
          id: rentalId,
        },
      },
      context,
      '{ id driverId carId rentalPeriods { id numDays } }',
    )
  })
}
function approveRental(rentalId, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const rental = yield getRental(rentalId, context)
    yield authorizeChargeToDriver(rental, context)
    const approvedRental = yield context.rentals.mutation.approveRental(
      {
        data: {
          rentalId: rentalId,
        },
      },
      context,
      `{
        id
        status
    }`,
    )
    yield context.cars.mutation.updateCar(
      {
        where: {
          id: rental.carId,
        },
        data: {
          status: 'RENTED',
        },
      },
      context,
      '{ id }',
    )
    return approvedRental
  })
}
function authorizeChargeToDriver(rental, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const [driver, owner, payment] = yield Promise.all([
      context.users.query.driver(
        {
          where: {
            id: rental.driverId,
          },
        },
        context,
        '{ id user { id } }',
      ),
      getRentalCarOwner(rental, context),
      context.payments.query.rentalPayment(
        { where: { rentalPeriodId: rental.rentalPeriods[0].id } },
        context,
        '{ id grandTotalInCents }',
      ),
    ])
    yield context.payments.mutation.authorizeBookingPayment(
      {
        data: {
          driverUserId: driver.user.id,
          ownerUserId: owner.user.id,
          rentalId: rental.id,
          rentalPeriodId: rental.rentalPeriods[0].id,
          rentalDescription: `Driver # ${driver.id} rents car # ${
            rental.carId
          } from owner # ${owner.id} for ${
            rental.rentalPeriods[0].numDays
          } day(s) at $${payment.grandTotalInCents / 100}`,
          depositDescription: `Driver # ${driver.id} rents car # ${
            rental.carId
          } from owner # ${owner.id} for ${
            rental.rentalPeriods[0].numDays
          } day(s) at $${payment.grandTotalInCents /
            100} Security deposit for rental $200`,
        },
      },
      context,
      '{ payment { id } deposit { id } }',
    )
  })
}
function getRentalCarOwner(rental, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const car = yield context.cars.query.car(
      {
        where: {
          id: rental.carId,
        },
      },
      context,
      '{ id ownerId }',
    )
    return yield context.users.query.owner(
      {
        where: {
          id: car.ownerId,
        },
      },
      context,
      '{ id user { id } }',
    )
  })
}
function isCurrentUserCarOwner(carId, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const car = yield context.cars.query.car(
      {
        where: {
          id: carId,
        },
      },
      context,
      '{ id ownerId }',
    )
    return context.currentUser.owner.id === car.ownerId
  })
}
exports.isCurrentUserCarOwner = isCurrentUserCarOwner
function getOtherDriverRentals(args, ctx) {
  return __awaiter(this, void 0, void 0, function*() {
    const { rentalId } = args
    const rental = yield ctx.rentals.query.rental(
      {
        where: {
          id: rentalId,
        },
      },
      ctx,
      `{
        driverId
    }`,
    )
    const otherRentals = yield ctx.rentals.query.rentals(
      {
        where: {
          driverId: rental.driverId,
          id_not: rentalId,
          status_in: ['APPLIED', 'APPLIED_NOT_VERIFIED'],
        },
      },
      ctx,
      '{ id carId }',
    )
    const drivers = yield ctx.users.query.users(
      {
        where: {
          driver: {
            id_in: rental.driverId,
          },
        },
      },
      ctx,
      `{
        id email phone firstName 
    }`,
    )
    return {
      driver: drivers.length === 0 ? null : drivers[0],
      otherRentals,
    }
  })
}
function notifyRejectedOwners(notifyOwnersInput, ctx) {
  return __awaiter(this, void 0, void 0, function*() {
    return yield notifyOwnersInput.otherRentals.map((o, i) =>
      __awaiter(this, void 0, void 0, function*() {
        const car = yield ctx.cars.query.car(
          { where: { id: o.carId } },
          ctx,
          `{id ownerId dailyPriceInCents monthlyPriceInCents weeklyPriceInCents}`,
        )
        const owner = yield ctx.users.query.users(
          {
            where: {
              owner: {
                id: car.ownerId,
              },
            },
            first: 1,
          },
          ctx,
          `{
            id email phone firstName
        }`,
        )
        const price = yield ctx.payments.query.getPriceBreakdown(
          {
            where: {
              dailyPriceInCents: car.dailyPriceInCents,
              monthlyPriceInCents: car.monthlyPriceInCents,
              rentalDays: o.currentRentalPeriod.numDays,
              weeklyPriceInCents: car.weeklyPriceInCents,
            },
          },
          ctx,
          `{basePriceAfterLongRentalsDiscount}`,
        )
        const rentalAmount = Math.round(
          price.basePriceAfterLongRentalDiscounts * 0.9,
        )
        const variables = {
          driverName: notifyOwnersInput.driver.firstName,
          ownerEmail: owner[0].email,
          ownerName: owner[0].firstName,
          rentalAmount,
        }
        if (i >= notifyOwnersInput.otherRentals.length) {
          return
        }
        sendNotificationsToOwners(variables)
      }),
    )
  })
}
const sendNotificationsToOwners = variables =>
  __awaiter(this, void 0, void 0, function*() {
    const query = `mutation notifyRejectedOwners($ownerName:String!,$ownerEmail:String!,$driverName:String!,rentalAmount:String!){createOwnerLostBidEmail(ownerName:$ownerName,driverName:$driverName,ownerEmail:$ownerEmail,rentalAmount:$rentalAmount){success}}`
    try {
      return yield apolloFetch({ query, variables })
    } catch (err) {
      console.error(err)
    }
  })
