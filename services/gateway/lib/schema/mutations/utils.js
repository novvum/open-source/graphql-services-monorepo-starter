'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const RentalContractsModel_1 = require('../../models/RentalContractsModel')
function getCurrentRentalInsurance(rental, ctx) {
  return __awaiter(this, void 0, void 0, function*() {
    const insurances = yield ctx.insurance.query.insurances(
      {
        first: 1,
        where: {
          rentalId: rental.id,
          carId: rental.carId,
        },
        orderBy: 'createdAt_DESC',
      },
      ctx,
      '{ id startDateTime status }',
    )
    if (insurances.length === 0) {
      return null
    }
    return insurances[0]
  })
}
exports.getCurrentRentalInsurance = getCurrentRentalInsurance
function getLastRentalPeriod(periods) {
  if (periods.length === 0) {
    return null
  }
  return periods.reduce((latestPeriod, currentPeriod) => {
    if (new Date(latestPeriod.endDate) > new Date(currentPeriod.endDate)) {
      return latestPeriod
    } else {
      return currentPeriod
    }
  }, periods[0])
}
exports.getLastRentalPeriod = getLastRentalPeriod
function generateContractFromRentalPeriod(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    try {
      const { driverId, rentalPeriod } = args
      const fetchedRentalPeriod = yield context.rentals.query.rentalPeriod(
        {
          where: {
            id: rentalPeriod.id,
          },
        },
        context,
        '{ id carId paymentId numDays startDate rental { id }  }',
      )
      const rentalPeriodDetails = {
        id: fetchedRentalPeriod.id,
        carId: fetchedRentalPeriod.carId,
        paymentId: fetchedRentalPeriod.paymentId,
        numDays: fetchedRentalPeriod.numDays,
        startDate: fetchedRentalPeriod.startDate,
        rentalId: fetchedRentalPeriod.rental.id,
      }
      const contractsModel = new RentalContractsModel_1.default(context)
      yield contractsModel.uploadRentalPeriodContract(
        { driverId, rentalPeriodDetails },
        context,
      )
    } catch (e) {
      console.error(e)
    }
  })
}
exports.generateContractFromRentalPeriod = generateContractFromRentalPeriod
