const moment = require('moment')

export class AuthError extends Error {
  constructor() {
    super('Not authorized')
  }
}
export const loadEnv = env => {
  if (process.env.UP_STAGE === 'production') {
    if (process.env[`${env}_PROD`]) {
      return process.env[`${env}_PROD`]
    }
  }
  return process.env[env]
}

export function optionalChaining(func) {
  try {
    return func()
  } catch (e) {
    return undefined
  }
}
