export declare const makeResponseError: (
  args: {
    message: string
    code: any
  },
) => Error
