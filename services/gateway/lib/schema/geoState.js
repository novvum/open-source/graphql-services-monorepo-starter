'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.getGeoStates = api => {
  return api.get('/db/geo/states').then(({ data }) => {
    return data
  })
}
exports.linkTypeDefs = `
    extend type Query {
      geoStates: [GeoState]
    }
`
exports.resolvers = mergeInfo => ({
  Query: {
    geoStates: (_, args, context, info) => {
      return exports.getGeoStates(context.legacyApi)
    },
  },
})
