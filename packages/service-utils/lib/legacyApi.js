'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
var __rest =
  (this && this.__rest) ||
  function(s, e) {
    var t = {}
    for (var p in s)
      if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p]
    if (s != null && typeof Object.getOwnPropertySymbols === 'function')
      for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++)
        if (e.indexOf(p[i]) < 0) t[p[i]] = s[p[i]]
    return t
  }
Object.defineProperty(exports, '__esModule', { value: true })
const axios_1 = require('axios')
class LegacyApi {
  constructor(args) {
    this.baseUri = args.baseUri
    var instance = axios_1.default.create({
      baseURL: this.baseUri,
      headers: { 'X-Access-Token': args.token || '' },
    })
    this.apiCaller = instance
    this.setToken(args.token)
  }
  setToken(newToken) {
    this.token = newToken
    if (newToken) {
      // Alter defaults after instance has been created
      this.apiCaller.defaults.headers['X-Access-Token'] = newToken
    }
  }
  getBaseUri() {
    return this.baseUri
  }
  get(uri, queryParams) {
    return __awaiter(this, void 0, void 0, function*() {
      const res = yield this.apiCaller.get(
        `${uri}${typeof queryParams === 'undefined' ? '' : `?${queryParams}`}`,
      )
      return res.data
    })
  }
  post(uri, opts = {}) {
    return __awaiter(this, void 0, void 0, function*() {
      const { body } = opts,
        rest = __rest(opts, ['body'])
      return (yield this.apiCaller.post(
        uri,
        opts.body,
        Object.assign({}, rest),
      )).data
    })
  }
  put(uri, opts = {}) {
    return __awaiter(this, void 0, void 0, function*() {
      const { body } = opts,
        rest = __rest(opts, ['body'])
      return (yield this.apiCaller.put(uri, body, Object.assign({}, rest))).data
    })
  }
  patch(uri) {
    return this.apiCaller.patch(uri)
  }
}
LegacyApi.SERVER_DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss'
exports.LegacyApi = LegacyApi
//# sourceMappingURL=legacyApi.js.map
