'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const graphql_tools_1 = require('graphql-tools')
const graphql_import_1 = require('graphql-import')
var path = require('path')
let typeDefs = `
  type Query {
    _: Boolean
  }
  type Mutation {
    _: Boolean
  }
`
if (
  path.join(__dirname, './generated/app.graphql') !== '/generated/app.graphql'
) {
  typeDefs = graphql_import_1.importSchema(
    path.join(__dirname, './generated/app.graphql'),
  )
}
const schema = graphql_tools_1.makeExecutableSchema({
  typeDefs: typeDefs,
  resolverValidationOptions: {
    requireResolversForResolveType: false,
  },
})
exports.default = schema
//# sourceMappingURL=schema.js.map
