'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
class AuthError extends Error {
  constructor() {
    super('Not authorized')
  }
}
exports.AuthError = AuthError
