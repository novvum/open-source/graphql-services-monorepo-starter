'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const utils_1 = require('./utils')
const RentalOverview_1 = require('../RentalOverview')
const requestBooking_1 = require('./requestBooking')
const moment = require('moment')
exports.linkTypeDefs = `
    input ExtendRentalInput {
        rentalId: ID!
        rentalDays: Int!
        couponCode: String
    }
    type ExtendRentalPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        rentalPeriod: RentalPeriod
    }
    extend type Mutation {
        extendRental(data: ExtendRentalInput!): ExtendRentalPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    extendRental(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const { rental, driverUser, car, ownerUser } = yield getRequiredData(
            context,
            args,
          )
          validateRequest({ rental, driver: driverUser.driver, car })
          const createdRentalPeriod = yield extendRental(
            { rental, input: args.data, car, driverUser, ownerUser },
            context,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
            rentalPeriod: createdRentalPeriod,
          }
        } catch (e) {
          return {
            code: lib_1.ResponseCodes.REQUEST_FAILED,
            success: false,
            message: e.message,
            rental: null,
          }
        }
      })
    },
  },
})
function extendRental(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { rental, input, car, driverUser, ownerUser } = args
    const previousRentalPeriod = utils_1.getLastRentalPeriod(
      rental.rentalPeriods,
    )
    const rentalOverview = yield RentalOverview_1.getRentalOverview(
      rental.id,
      context,
    )
    const createdRentalPeriod = yield createRentalExtension(
      {
        rental,
        input,
        previousRentalPeriod,
      },
      context,
    )
    const extensionPayment = yield createRentalExtensionPayment(
      {
        input: input,
        car,
        previousRentalPeriod,
        driverUser,
        createdRentalPeriod,
        ownerUser,
        isRentalLate: checkLateRental(rentalOverview, context),
        rental,
      },
      context,
    )
    const updatedRentalPeriod = yield updateRentalPeriodWithPayment(
      { rentalPeriod: createdRentalPeriod, payment: extensionPayment },
      context,
    )
    updateRentalStatus(rental, context)
    utils_1.generateContractFromRentalPeriod(
      { driverId: driverUser.driver.id, rentalPeriod: updatedRentalPeriod },
      context,
    )
    return updatedRentalPeriod
  })
}
function updateRentalPeriodWithPayment(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { rentalPeriod, payment } = args
    return yield context.rentals.mutation.updateRentalPeriod(
      {
        where: {
          id: rentalPeriod.id,
        },
        data: {
          paymentId: payment.id,
        },
      },
      context,
    )
  })
}
function createRentalExtension(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { rental, input, previousRentalPeriod } = args
    return yield context.rentals.mutation.createRentalPeriod(
      {
        data: {
          type: 'EXTENSION',
          carId: rental.carId,
          rental: {
            connect: {
              id: rental.id,
            },
          },
          numDays: input.rentalDays,
          startDate: lib_1.getDate(previousRentalPeriod.endDate),
          endDate: moment(new Date(previousRentalPeriod.endDate)).add(
            input.rentalDays,
            'days',
          ),
        },
      },
      context,
      '{ id carId numDays startDate endDate }',
    )
  })
}
function getRequiredData(context, args) {
  return __awaiter(this, void 0, void 0, function*() {
    const driverUser = (yield context.users.query.viewer(
      null,
      context,
      '{ me { id  driver { id blockedFromBooking backgroundCheck { id verificationStatus } } } }',
    )).me
    const rental = yield context.rentals.query.rental(
      {
        where: {
          id: args.data.rentalId,
        },
      },
      context,
      '{ id carId driverId rentalPeriods { id type startDate endDate numDays } }',
    )
    const car = yield context.cars.query.car(
      {
        where: {
          id: rental.carId,
        },
      },
      context,
      `{
                    id
                    status
                    ownerId
                    dailyPriceInCents
                    weeklyPriceInCents
                    monthlyPriceInCents
                    location {
                        id
                        state
                    }
                }`,
    )
    const ownerUser = (yield context.users.query.owner(
      {
        where: {
          id: car.ownerId,
        },
      },
      context,
      '{ id user { id owner { id } } }',
    )).user
    return { rental, driverUser, car, ownerUser }
  })
}
function validateRequest(args) {
  const { rental, driver, car } = args
  if (
    rental.driverId !== driver.id ||
    !requestBooking_1.canDriverRent(driver)
  ) {
    throw lib_1.makeResponseError({
      code: lib_1.ResponseCodes.BAD_REQUEST,
      message:
        'You are not authorized to extend at this time. Please contact support for further details.',
    })
  }
  if (!isCarExtendable(car || {})) {
    return {
      code: lib_1.ResponseCodes.BAD_REQUEST,
      success: false,
      message: 'This car cannot be extended.',
    }
  }
}
function createRentalExtensionPayment(data, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const {
      input,
      car,
      previousRentalPeriod,
      createdRentalPeriod,
      driverUser,
      ownerUser,
      isRentalLate,
      rental,
    } = data
    try {
      const extensionPayment = (yield context.payments.mutation.extendBookingPayment(
        {
          data: {
            priceInfo: {
              couponCode: input.couponCode,
              rentalDays: input.rentalDays,
              dailyPriceInCents: car.dailyPriceInCents,
              weeklyPriceInCents: car.weeklyPriceInCents,
              monthlyPriceInCents: car.monthlyPriceInCents,
              showDeposit: false,
            },
            previousRentalPeriodId: previousRentalPeriod.id,
            newRentalPeriodId: createdRentalPeriod.id,
            driverUserId: driverUser.id,
            ownerUserId: ownerUser.id,
            isLate: isRentalLate,
            lateFeeDescription: `Driver # ${
              driverUser.driver.id
            } pays for late fee. (Rental # ${rental.id})`,
            extensionDescription: `Driver # ${
              driverUser.driver.id
            } extends rental # ${rental.id} (extension # ${
              createdRentalPeriod.id
            }, car # ${car.id}) from owner # ${ownerUser.owner.id} for ${
              createdRentalPeriod.numDays
            } day(s)`,
          },
        },
        context,
        `{
            extension {
                payment {
                    id
                }
            }
        }`,
      )).extension.payment
      return extensionPayment
    } catch (e) {
      yield context.rentals.mutation.deleteRentalPeriod(
        {
          where: {
            id: createdRentalPeriod.id,
          },
        },
        context,
      )
      throw lib_1.makeResponseError({
        code: lib_1.ResponseCodes.BAD_REQUEST,
        message: e.message,
      })
    }
  })
}
function checkLateRental(rentalOverview, context) {
  return rentalOverview.daysRemaining < 0
}
function isCarExtendable(car) {
  return car.status === 'RENTED'
}
function updateRentalStatus(rental, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const overview = yield RentalOverview_1.getRentalOverview(
      rental.id,
      context,
    )
    const response = yield context.rentals.mutation.updateRental(
      {
        where: {
          id: rental.id,
        },
        data: {
          status: overview.daysRemaining < 0 ? 'LATE' : 'ACTIVE',
        },
      },
      context,
      '{ id status}',
    )
    rental.status === 'LATE' &&
      updateDriverNotificationStatus(rental.id, response.status)
    return response
  })
}
function updateDriverNotificationStatus(rentalId, rentalStatus) {
  return __awaiter(this, void 0, void 0, function*() {
    const { createApolloFetch } = require('apollo-fetch')
    const uri = 'https://api.graph.cool/simple/v1/cjgefci8x19990196lpbbhc2h'
    const apolloFetch = createApolloFetch({ uri })
    const variables = {
      rentalId,
      rentalStatus,
    }
    const query = `mutation updateLateDrivers($rentalId: String! $rentalStatus: String!){
        updateLateDriverRentals(rentalId: $rentalId, rentalStatus: $rentalStatus){
            success
        }
    }`
    return yield apolloFetch({ query })
  })
}
exports.updateDriverNotificationStatus = updateDriverNotificationStatus
