'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const graphql_yoga_1 = require('graphql-yoga')
const getContext_1 = require('../getContext')
const schema_1 = require('../schema')
const config_1 = require('../config')
const mocks_1 = require('../mocks')
const apolloEngine_1 = require('../utils/apolloEngine')
const getYogaConfig_1 = require('../utils/getYogaConfig')
// const timber = require('timber')
// const transport = new timber.transports.HTTPS(process.env.INTERNAL_TIMBER_KEY);
// timber.install(transport);
// server.use(timber.middlewares.express());
exports.default = (yogaConfig, path) => {
  const server = new graphql_yoga_1.GraphQLServer({
    schema:
      config_1.default.GRAPHQL_ENABLE_MOCKS === 'true'
        ? schema_1.default.getMockedSchema(mocks_1.mocks)
        : schema_1.default.getSchema(),
    context: getContext_1.getContext,
  })
  server.express.use(getContext_1.context.users.auth.internalAccessMiddleware)
  server.express.use(getContext_1.context.users.auth.internalScopesMiddleware)
  server.createHttpServer(getYogaConfig_1.default(yogaConfig, path))
  if (!path) {
    apolloEngine_1.startApolloEngine({
      port: yogaConfig.port,
      serverType: 'internal',
      server,
    })
  }
  return server
}
