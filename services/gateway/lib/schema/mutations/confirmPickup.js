'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const approveRejectBooking_1 = require('./approveRejectBooking')
const utils_1 = require('./utils')
exports.linkTypeDefs = `
    input ConfirmPickupInput {
        rentalId: ID!
        carBackImage: Upload!
        carFrontImage: Upload!
        carGasLevelImage: Upload!
        carMileageImage: Upload!
        carPassengerSideImage: Upload!
        carDriverSideImage: Upload!
        isDriverIdentified: Boolean!
    }
    input AdminConfirmPickupInput {
        rentalId: ID!
        carBackImage: Upload
        carFrontImage: Upload
        carGasLevelImage: Upload
        carMileageImage: Upload
        carPassengerSideImage: Upload
        carDriverSideImage: Upload
        isDriverIdentified: Boolean
        pickedUpAt: DateTime!
    }
    type ConfirmPickupPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        rental: Rental
    }
    extend type Mutation {
        confirmPickup(data: ConfirmPickupInput!): ConfirmPickupPayload
        adminConfirmPickup(data: AdminConfirmPickupInput!): ConfirmPickupPayload
        adminUploadPickupPhotos(insuranceId: ID!, files: [Upload!]!): ConfirmPickupPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    confirmPickup(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          yield approveRejectBooking_1.validateRentalOwnerRequest(
            args.data.rentalId,
            context,
          )
          const updatedRental = yield pickupRental(
            {
              input: args.data,
            },
            context,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            rental: updatedRental,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            rental: null,
          }
        }
      })
    },
    adminConfirmPickup(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const updatedRental = yield pickupRental(
            {
              input: args.data,
              adminInput: args.data,
            },
            context,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            rental: updatedRental,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            rental: null,
          }
        }
      })
    },
    adminUploadPickupPhotos(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const preInsuranceId = (yield context.insurance.query.insurance(
            {
              where: {
                id: args.insuranceId,
              },
            },
            context,
            '{ id pre { id } }',
          )).pre.id
          const files = args.files.map(file => ({
            type: 'GENERAL',
            file,
          }))
          yield uploadPreInsurancePhotos({ preInsuranceId, files }, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      })
    },
  },
})
function uploadPreInsuranceFiles(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const typeMap = {
      CAR_BACK: 'carBackImage',
      CAR_FRONT: 'carFrontImage',
      CAR_GAS_LEVEL: 'carGasLevelImage',
      CAR_MILEAGE: 'carMileageImage',
      CAR_PASSENGER_SIDE: 'carPassengerSideImage',
      CAR_DRIVER_SIDE: 'carDriverSideImage',
    }
    const keys = Object.keys(typeMap)
    const files = keys
      .map(key => ({
        type: key,
        file: args.data[typeMap[key]],
      }))
      .filter(file => file.file && true)
    const preInsuranceId = args.insurance.pre.id
    yield uploadPreInsurancePhotos({ files, preInsuranceId }, context)
  })
}
function uploadPreInsurancePhotos(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { files, preInsuranceId } = args
    const uploadedFiles = yield Promise.all(
      files.map(file =>
        __awaiter(this, void 0, void 0, function*() {
          return {
            type: file.type,
            fileId: (yield uploadCarPickupFile(file.file, context)).id,
          }
        }),
      ),
    )
    for (let file of uploadedFiles) {
      yield context.insurance.mutation.createPreInsurancePhoto(
        {
          data: {
            fileId: file.fileId,
            type: file.type,
            preInsurance: {
              connect: {
                id: preInsuranceId,
              },
            },
          },
        },
        context,
      )
    }
  })
}
function uploadCarPickupFile(file, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const uploadedFile = yield context.files.mutation.uploadFile(
      {
        data: {
          file,
          type: 'CAR_PICKUP',
        },
      },
      context,
      '{ id }',
    )
    return uploadedFile
  })
}
function pickupRental(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { input, adminInput } = args
    const rental = yield context.rentals.query.rental(
      {
        where: {
          id: input.rentalId,
        },
      },
      context,
      '{ id driverId disallowExtensions carId currentRentalPeriod { id type numDays } }',
    )
    yield updateInsuranceEvidence({ input, rental }, context)
    const pickedUpAt =
      lib_1.optionalChaining(() => adminInput.pickedUpAt) ||
      lib_1.getDate(new Date())
    const updatedRental = yield context.rentals.mutation.updateRental(
      {
        where: {
          id: rental.id,
        },
        data: {
          status: 'ACTIVE',
        },
      },
      context,
      '{ id status rentalPeriods { id startDate } }',
    )
    return updatedRental
  })
}
function updateInsuranceEvidence(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { input, rental } = args
    const insurance = yield utils_1.getCurrentRentalInsurance(rental, context)
    const preInsuranceData = {
      isDriverIdentified: input.isDriverIdentified,
    }
    const updatedInsurance = yield context.insurance.mutation.updateInsurance(
      {
        where: {
          id: insurance.id,
        },
        data: {
          pre: {
            upsert: {
              create: preInsuranceData,
              update: preInsuranceData,
            },
          },
        },
      },
      context,
      '{ id pre { id } }',
    )
    yield uploadPreInsuranceFiles(
      { data: input, insurance: updatedInsurance },
      context,
    )
  })
}
