'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type Owner {
    claims(filter: ClaimWhereInput, orderBy: ClaimOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Claim!]
  }
  extend type Claim {
    owner: Owner
}
`
exports.resolvers = mergeInfo => ({
  Claim: {
    owner: {
      fragment: `fragment OwnerFragment on Claim { ownerId }`,
      resolve(parent, _, context, info) {
        const id = parent.ownerId
        return mergeInfo.delegate(
          'query',
          'owner',
          {
            where: {
              id: id,
            },
          },
          context,
          info,
        )
      },
    },
  },
  Owner: {
    claims: {
      fragment: `fragment ClaimFragment on Owner { id }`,
      resolve(parent, args, context, info) {
        const id = parent.id
        return mergeInfo.delegate(
          'query',
          'claims',
          Object.assign({}, args, {
            where: {
              ownerId: id,
            },
          }),
          context,
          info,
        )
      },
    },
  },
})
