import { addMockFunctionsToSchema } from 'graphql-tools'
import userMocks from './usersMocks'
import { IMocks } from 'graphql-tools/dist/Interfaces'

const objectAssignDeep = require(`object-assign-deep`)

function randomDate(start, end) {
  return new Date(
    start.getTime() + Math.random() * (end.getTime() - start.getTime()),
  )
}

export const scalarMocks = {
  DateTime: () => randomDate(new Date(2012, 0, 1), new Date()),
}

export const mocks: IMocks = objectAssignDeep({}, scalarMocks, userMocks)
