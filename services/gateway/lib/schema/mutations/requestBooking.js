'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const getCarPriceBreakdown_1 = require('../getCarPriceBreakdown')
exports.linkTypeDefs = `
    input RequestBookingInput {
        carId: ID!
        rentalDays: Int!
        startDate: DateTime!
        couponCode: String
        tnc: Boolean
        tos: Boolean
    }
    type RequestBookingPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        rental: Rental
    }
    extend type Mutation {
        """Car must be available, creates a rental with status APPLIED_NOT_VERIFIED if driver is not verified, or just APPLIED if they are ready to go"""
        requestBooking(data: RequestBookingInput!): RequestBookingPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    requestBooking(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const { car, driverUser } = yield getRequiredData(context, args)
          validateRequest({ driver: driverUser.driver, car })
          const rental = yield requestBooking(
            { driverUser, car, input: args.data },
            context,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
            rental,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.REQUEST_FAILED,
            success: false,
            message: e.message,
            rental: null,
          }
        }
      })
    },
  },
})
function requestBooking(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { driverUser, car, input } = args
    const createdRental = yield createRental(
      { driver: driverUser.driver, car, input },
      context,
    )
    const payment = yield createRentalPayment(
      { input, car, createdRental, driverUser },
      context,
    )
    const rental = yield updateRentalWithPaymentInfo(
      { rental: createdRental, payment },
      context,
    )
    return rental
  })
}
exports.requestBooking = requestBooking
function getRequiredData(context, args) {
  return __awaiter(this, void 0, void 0, function*() {
    const driverUser = (yield context.users.query.viewer(
      null,
      context,
      '{ me { id  driver { id blockedFromBooking backgroundCheck { id verificationStatus } } } }',
    )).me
    const car = yield context.cars.query.car(
      {
        where: {
          id: args.data.carId,
        },
      },
      context,
      `{
        id
        status
        dailyPriceInCents
        weeklyPriceInCents
        monthlyPriceInCents
        location {
            id
            state
        }
    }`,
    )
    return { car, driverUser }
  })
}
function validateRequest(args) {
  const { driver, car } = args
  if (!canDriverRent(driver)) {
    throw lib_1.makeResponseError({
      code: lib_1.ResponseCodes.BAD_REQUEST,
      message:
        'You are not authorized to rent vehicles at this time. Please contact support for further details.',
    })
  }
  if (!isCarAvailable(car || {})) {
    throw lib_1.makeResponseError({
      code: lib_1.ResponseCodes.BAD_REQUEST,
      message: 'This car is not available to rent.',
    })
  }
}
function updateRentalWithPaymentInfo(data, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { rental, payment } = data
    return yield context.rentals.mutation.updateRental(
      {
        where: {
          id: rental.id,
        },
        data: {
          depositId: payment.deposit ? payment.deposit.id : undefined,
          rentalPeriods: {
            update: {
              where: {
                id: rental.rentalPeriods[0].id,
              },
              data: {
                paymentId: payment.payment.id,
              },
            },
          },
        },
      },
      context,
      `
    {
        id
        carId
        status
        createdAt
        updatedAt
        disallowExtensions
        depositId
        rentalPeriods {
            id
            createdAt
            updatedAt
            numDays
            startDate
            endDate
            type
            paymentId
        }
    }`,
    )
  })
}
function createRentalPayment(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { input, car, createdRental, driverUser } = args
    return yield context.payments.mutation.requestBookingPayment(
      {
        data: {
          priceInfo: {
            showDeposit: yield getCarPriceBreakdown_1.checkDepositRequired(
              { driverUserId: driverUser.id, carId: car.id },
              context,
            ),
            couponCode: input.couponCode,
            rentalDays: input.rentalDays,
            dailyPriceInCents: car.dailyPriceInCents,
            weeklyPriceInCents: car.weeklyPriceInCents,
            monthlyPriceInCents: car.monthlyPriceInCents,
          },
          rentalId: createdRental.id,
          userId: driverUser.id,
          car: {
            state: car.location.state,
          },
          payment: {
            rentalPeriodId: createdRental.rentalPeriods[0].id,
          },
        },
      },
      context,
      `{
        deposit {
            id
        }
        payment {
            id
        }
    }`,
    )
  })
}
function createRental(data, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { driver, input, car } = data
    let rentalStatus
    if (driver.backgroundCheck.verificationStatus === 'VERIFIED') {
      rentalStatus = 'APPLIED'
    } else {
      rentalStatus = 'APPLIED_NOT_VERIFIED'
    }
    const createdRental = yield context.rentals.mutation.applyDriverToRental(
      {
        data: {
          carId: car.id,
          driverId: driver.id,
          status: rentalStatus,
          startDateAt: lib_1.getDate(input.startDate),
          numDays: input.rentalDays,
          rentalContractId: 'asdf',
        },
      },
      context,
      `{
                    id
                    carId
                    status
                    createdAt
                    updatedAt
                    disallowExtensions
                    rentalPeriods {
                      id
                      createdAt
                      updatedAt
                      numDays
                      startDate
                      endDate
                      type
                    }
                  }`,
    )
    return createdRental
  })
}
function canDriverRent(driver) {
  if (driver.blockedFromBooking) {
    return false
  }
  return ['APPLIED', 'VERIFIED'].includes(
    driver.backgroundCheck.verificationStatus,
  )
}
exports.canDriverRent = canDriverRent
function isCarAvailable(car) {
  return car.status === 'AVAILABLE'
}
