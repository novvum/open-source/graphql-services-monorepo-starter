'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
function getYogaConfig(yogaConfig, path = '/') {
  return Object.assign(
    { tracing: true, cacheControl: true },
    yogaConfig || {},
    { playground: false },
  )
}
exports.default = getYogaConfig
