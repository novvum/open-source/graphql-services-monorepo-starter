'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
var __rest =
  (this && this.__rest) ||
  function(s, e) {
    var t = {}
    for (var p in s)
      if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p]
    if (s != null && typeof Object.getOwnPropertySymbols === 'function')
      for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++)
        if (e.indexOf(p[i]) < 0) t[p[i]] = s[p[i]]
    return t
  }
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
    extend type Query {
      offersConnection(filter: CarWhereInput, orderBy: CarOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int, sortBy: OffersSortByEnum): CarConnection!
      offers(filter: CarWhereInput, orderBy: CarOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Car]!
      offer(offerId: ID!): Car
    }
`
const CAR_ON_MARKET_STATUSES = ['AVAILABLE']
exports.resolvers = mergeInfo => ({
  Query: {
    offersConnection(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        const _a = yield getProcessedCarData(args, context),
          { carIds } = _a,
          rest = __rest(_a, ['carIds'])
        const variableValues = {
          first: args.first * 2,
          where: {
            id_in: carIds,
          },
        }
        const edges = (yield mergeInfo.delegate(
          'query',
          'carsConnection',
          variableValues,
          context,
          Object.assign({}, info, { variableValues }),
        )).edges.sort((a, b) => {
          return carIds.indexOf(a.node.id) - carIds.indexOf(b.node.id)
        })
        return Object.assign({}, rest, { edges })
      })
    },
    offers(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        return mergeInfo.delegate(
          'query',
          'cars',
          Object.assign({}, args, {
            where: {
              AND: [{ status_in: CAR_ON_MARKET_STATUSES }, args.filter || {}],
            },
          }),
          context,
          info,
        )
      })
    },
    offer(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        const offers = yield context.cars.query.cars(
          {
            where: {
              status_in: CAR_ON_MARKET_STATUSES,
              id: args.offerId,
            },
          },
          context,
          '{id status}',
        )
        if (offers.length === 0) {
          return null
        }
        return mergeInfo.delegate(
          'query',
          'car',
          Object.assign({}, args, {
            where: {
              id: offers[0].id,
            },
          }),
          context,
          info,
        )
      })
    },
  },
})
function getProcessedCarData(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { first, before, after, sortBy, filter } = args
    const allCarsConnection = yield context.cars.query.carsConnection(
      {
        where: {
          AND: [{ status_in: CAR_ON_MARKET_STATUSES }, filter || {}],
        },
      },
      context,
      '{ aggregate { count } edges { cursor node {id dailyPriceInCents location { id } } } }',
    )
    const sortedCars = allCarsConnection.edges.sort((a, b) => {
      switch (sortBy) {
        case 'RELEVANCE':
        case 'DISTANCE':
        case 'PRICE_ASC':
          return a.node.dailyPriceInCents - b.node.dailyPriceInCents
        case 'PRICE_DESC':
          return b.node.dailyPriceInCents - a.node.dailyPriceInCents
        default:
          return 0
      }
    })
    let startIdx = 0
    if (before) {
      const beforeIdx = sortedCars.findIndex(edge => edge.cursor === before)
      if (beforeIdx - first < 0) {
        startIdx = 0
      } else {
        startIdx = beforeIdx - first
      }
    }
    if (after) {
      const afterIdx = sortedCars.findIndex(edge => edge.cursor === after)
      startIdx = afterIdx
    }
    const pageInfo = {
      hasNextPage: startIdx + first < sortedCars.length - 1,
      hasPreviousPage: startIdx - first >= 0,
      startCursor: sortedCars[startIdx].cursor,
      endCursor: sortedCars[startIdx + first]
        ? sortedCars[startIdx + first].cursor
        : sortedCars[sortedCars.length - 1].cursor,
    }
    const carIds = sortedCars
      .slice(startIdx, startIdx + first)
      .map(car => car.node.id)
    return {
      carIds,
      pageInfo,
      aggregate: allCarsConnection.aggregate,
    }
  })
}
