import * as listCar from './listCar'

export default {
  schemas: [listCar.linkTypeDefs],
  resolvers: [listCar.resolvers],
}
