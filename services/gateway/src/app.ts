import publicServer from './publicServer'
import config from './config'

const serverConfig = {
  port: config.PORT,
  uploads: {
    maxFiles: 15,
  },
  playground: '/',
}

/**
 * The server.
 *
 * @class Server
 */
export class Server {
  /**
   * Bootstrap the application.
   *
   * @class Server
   * @method bootstrap
   * @static
   */
  public static bootstrap(): Server {
    return new Server()
  }

  /**
   * Constructor.
   *
   * @class Server
   * @constructor
   */
  constructor() {
    return publicServer(serverConfig)
  }
}
