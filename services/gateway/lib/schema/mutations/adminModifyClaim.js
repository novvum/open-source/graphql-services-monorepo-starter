'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
exports.linkTypeDefs = `
    type AdminModifyClaimPayload {
        code: String
        success: Boolean
        message: String
        payload: AdminClaimPayload
    }

    extend type Mutation {
        adminModifyClaim(claim: ClaimUpdateInput!, userId: ID!): AdminModifyClaimPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    adminModifyClaim(_, args, ctx, info) {
      return __awaiter(this, void 0, void 0, function*() {
        const { userId, claim } = args
        try {
          const user = yield ctx.users.query.user(
            {
              where: {
                id: userId,
              },
            },
            ctx,
            `{
            id
            firstName
            lastName
            email
          }`,
          )
          const payload = yield ctx.admin.mutation.adminUpdateClaim(
            {
              data: claim.data,
              by: {
                id: userId,
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
              },
            },
            ctx,
            `{
            claim {
              id
              status
              rentalId
              ownerId
              carId
              insuranceId
              submittedBy {
                userId
                submittedVia
              }
            }
            by {
              id
              firstName
              lastName
              email
            }
          }`,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            payload,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.REQUEST_FAILED,
            success: false,
            message: e.message,
          }
        }
      })
    },
  },
})
