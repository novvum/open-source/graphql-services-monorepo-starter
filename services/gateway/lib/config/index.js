'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const config = {
  test: {
    legacyApiUrl: 'https://rest.hyrecar.com',
    LEGACY_DB_HOST: 'staging-hyrecar.cck6vmheseot.us-west-1.rds.amazonaws.com',
    LEGACY_DB_NAME: 'hyrecar',
    apolloEngine: {
      internal: 'service:hc-graphql-internal-staging:3U7wvlmMFvdRU810vl6ZQQ',
      api: 'service:hc-graphql-staging:bIKLpPUECj0CQhYlqmBvPw',
    },
  },
  dev: {
    legacyApiUrl: 'https://staging.rest.hyrecar.com',
    LEGACY_DB_HOST: 'staging-hyrecar.cck6vmheseot.us-west-1.rds.amazonaws.com',
    LEGACY_DB_NAME: 'hyrecar',
    apolloEngine: {
      internal: 'service:hc-graphql-internal-staging:3U7wvlmMFvdRU810vl6ZQQ',
      api: 'service:hc-graphql-staging:bIKLpPUECj0CQhYlqmBvPw',
    },
  },
  staging: {
    legacyApiUrl: 'https://staging.rest.hyrecar.com',
    LEGACY_DB_HOST: 'staging-hyrecar.cck6vmheseot.us-west-1.rds.amazonaws.com',
    LEGACY_DB_NAME: 'hyrecar',
    apolloEngine: {
      internal: 'service:hc-graphql-internal-staging:3U7wvlmMFvdRU810vl6ZQQ',
      api: 'service:hc-graphql-staging:bIKLpPUECj0CQhYlqmBvPw',
    },
  },
  production: {
    legacyApiUrl: 'https://rest.hyrecar.com',
    LEGACY_DB_HOST: 'staging-hyrecar.cck6vmheseot.us-west-1.rds.amazonaws.com',
    LEGACY_DB_NAME: 'hyrecar',
    apolloEngine: {
      internal: 'service:hc-graphql-internal:YuRr16k_4HNfnD0_72gZvg',
      api: 'service:hc-graphql:bJUN5yU5Kw7gQjZsJbyK8w',
    },
  },
  ADMIN_PRISMA_ENDPOINT: `${
    process.env.GRAPHQL_PRISMA_BASE_URL
  }/admin-service/${process.env.GRAPHQL_STAGE}`,
  RATINGS_PRISMA_ENDPOINT: `${
    process.env.GRAPHQL_PRISMA_BASE_URL
  }/ratings-service/${process.env.GRAPHQL_STAGE}`,
  FILES_PRISMA_ENDPOINT: `${
    process.env.GRAPHQL_PRISMA_BASE_URL
  }/files-service/${process.env.GRAPHQL_STAGE}`,
  CARS_PRISMA_ENDPOINT: `${process.env.GRAPHQL_PRISMA_BASE_URL}/cars-service/${
    process.env.GRAPHQL_STAGE
  }`,
  USERS_PRISMA_ENDPOINT: `${
    process.env.GRAPHQL_PRISMA_BASE_URL
  }/users-service/${process.env.GRAPHQL_STAGE}`,
  RENTALS_PRISMA_ENDPOINT: `${
    process.env.GRAPHQL_PRISMA_BASE_URL
  }/rentals-service/${process.env.GRAPHQL_STAGE}`,
  PAYMENTS_PRISMA_ENDPOINT: `${
    process.env.GRAPHQL_PRISMA_BASE_URL
  }/payments-service/${process.env.GRAPHQL_STAGE}`,
  INSURANCE_PRISMA_ENDPOINT: `${
    process.env.GRAPHQL_PRISMA_BASE_URL
  }/insurance-service/${process.env.GRAPHQL_STAGE}`,
  CURRENT_TOS_VERSION: '2017.11',
  SERVER_TYPE: process.env.GRAPHQL_SERVER_TYPE,
  eventGatewayUrl: process.env.GRAPHQL_EVENT_GATEWAY_URL,
  PORT: parseInt(process.env.PORT || '4000', 10),
}
exports.default = Object.assign({}, config, config[process.env.GRAPHQL_STAGE], {
  GRAPHQL_ENABLE_MOCKS: process.env.GRAPHQL_ENABLE_MOCKS,
  GRAPHQL_STAGE: process.env.GRAPHQL_STAGE,
  GRAPHQL_PRISMA_SECRET: process.env.GRAPHQL_PRISMA_SECRET,
  GRAPHQL_LEGACY_DB_USERNAME: process.env.GRAPHQL_LEGACY_DB_USERNAME,
  GRAPHQL_LEGACY_DB_PASSWORD: process.env.GRAPHQL_LEGACY_DB_PASSWORD,
})
