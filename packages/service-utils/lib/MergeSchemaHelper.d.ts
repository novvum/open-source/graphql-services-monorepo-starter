import { IResolvers, MergeInfo, IMocks } from 'graphql-tools/dist/Interfaces'
import { GraphQLNamedType, GraphQLSchema } from 'graphql'
export declare class MergeSchemaHelper {
  schemas: Array<GraphQLSchema | string>
  onTypeConflict?: (
    left: GraphQLNamedType,
    right: GraphQLNamedType,
  ) => GraphQLNamedType
  resolvers?: Array<IResolvers | ((mergeInfo: MergeInfo) => IResolvers)>
  constructor()
  addSchema(schema: GraphQLSchema | string): void
  addSchemas(schemas: Array<GraphQLSchema | string>): void
  addResolver(
    resolvers: ((mergeInfo: MergeInfo) => IResolvers) | IResolvers,
  ): void
  addResolvers(
    resolvers: Array<((mergeInfo: MergeInfo) => IResolvers) | IResolvers>,
  ): void
  getSchema(): GraphQLSchema
  getMockedSchema(mocks: IMocks): GraphQLSchema
}
export { GraphQLSchema }
