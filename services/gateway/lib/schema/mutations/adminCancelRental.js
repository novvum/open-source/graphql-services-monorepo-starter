'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const switchCar_1 = require('./switchCar')
exports.linkTypeDefs = `
    input AdminCancelRentalInput {
        rentalId: ID!
        reason: RentalCancellationTypeEnum!
    }
    type AdminCancelRentalPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
    }
    extend type Mutation {
        adminCancelRental(where: AdminCancelRentalInput!): AdminCancelRentalPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    adminCancelRental(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const rental = yield context.rentals.query.rental(
            {
              where: {
                id: args.where.rentalId,
              },
            },
            context,
            '{ id status driverId disallowExtensions carId currentRentalPeriod { id numDays startDate endDate } }',
          )
          yield validateRequest({ rental }, context)
          const canceledRental = yield adminCancelRental(
            { input: args.where, rental },
            context,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            rental: canceledRental,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            rating: null,
          }
        }
      })
    },
  },
})
function validateRequest(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { rental } = args
    const CANCELABLE_STATUSES = ['PENDING_INSURANCE', 'PENDING_PICKUP']
    if (!CANCELABLE_STATUSES.includes(rental.status)) {
      throw lib_1.makeResponseError({
        message: `A rental with the status '${
          rental.status
        }' cannot be canceled.`,
        code: lib_1.ResponseCodes.BAD_REQUEST,
      })
    }
  })
}
function adminCancelRental(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { input, rental } = args
    switch (rental.status) {
      case 'PENDING_INSURANCE':
        return yield cancelRental({ rental, reason: input.reason }, context)
      case 'PENDING_PICKUP':
        return yield cancelInsuredRental(
          { rental, reason: input.reason },
          context,
        )
      default:
        return null
    }
  })
}
function cancelRental(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { rental, reason } = args
    try {
      yield context.payments.mutation.refundBookingPayment(
        {
          data: {
            rentalPeriodId: rental.currentRentalPeriod.id,
            rentalId: rental.id,
            reason: reason,
          },
        },
        context,
      )
    } catch (e) {
      console.error(e)
    }
    const updatedRental = yield context.rentals.mutation.updateRental(
      {
        where: {
          id: rental.id,
        },
        data: {
          status: 'CANCELLED',
          cancellationReason: reason,
        },
      },
      context,
      '{ id carId status }',
    )
    yield switchCar_1.relistCar(rental.carId, context)
    return updatedRental
  })
}
function cancelInsuredRental(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { rental, reason } = args
    yield switchCar_1.closeInsurance(rental, context)
    return yield cancelRental({ rental, reason }, context)
  })
}
