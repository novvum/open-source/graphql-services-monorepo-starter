import { makeExecutableSchema } from 'graphql-tools'
import { ServicesContext, GraphQLSchema } from 'service-utils/lib'
import { Prisma } from './generated/prisma'
import { resolvers } from './resolvers'
import { Binding as GeneratedBinding } from './generated/app'
import * as typeDefs from './generated/app.graphql'

export interface PrismaSampleContext extends ServicesContext {
  prismaSample: PrismaSampleBinding
}
export type PrismaSampleBindingConstructorArgs = {
  prismaEndpoint: string
  prismaSecret: string
  debug?: boolean
}
export class PrismaSampleBinding extends GeneratedBinding {
  db: Prisma
  schema: GraphQLSchema
  constructor(args: PrismaSampleBindingConstructorArgs) {
    const { prismaEndpoint, prismaSecret, debug = true } = args
    const schema = makeExecutableSchema({
      typeDefs,
      resolvers,
      resolverValidationOptions: {
        requireResolversForResolveType: false,
      } as any,
    })
    super({
      schema,
    })
    this.schema = schema
    this.db = new Prisma({
      endpoint: prismaEndpoint,
      secret: prismaSecret,
      debug,
    })
  }
}
