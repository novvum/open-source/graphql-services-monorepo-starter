import { importSchema } from 'graphql-import'
import * as path from 'path'

export const importGraphqlFile = (baseDir, relativePath) => {
  return importSchema(path.join(baseDir, relativePath))
}
