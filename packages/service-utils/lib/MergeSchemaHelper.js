'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const graphql_tools_1 = require('graphql-tools')
const graphql_1 = require('graphql')
exports.GraphQLSchema = graphql_1.GraphQLSchema
const objectAssignDeep = require(`object-assign-deep`)
class MergeSchemaHelper {
  constructor() {
    this.schemas = []
    this.resolvers = []
  }
  addSchema(schema) {
    this.schemas.push(schema)
  }
  addSchemas(schemas) {
    this.schemas.push(...schemas)
  }
  addResolver(resolvers) {
    this.resolvers.push(resolvers)
  }
  addResolvers(resolvers) {
    this.resolvers.push(...resolvers)
  }
  getSchema() {
    return graphql_tools_1.mergeSchemas({
      schemas: this.schemas,
      resolvers: this.resolvers,
    })
  }
  getMockedSchema(mocks) {
    const mockedSchema = this.getSchema()
    graphql_tools_1.addMockFunctionsToSchema({
      schema: mockedSchema,
      mocks,
    })
    return mockedSchema
  }
}
exports.MergeSchemaHelper = MergeSchemaHelper
//# sourceMappingURL=MergeSchemaHelper.js.map
