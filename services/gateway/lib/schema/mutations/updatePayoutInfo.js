'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
exports.linkTypeDefs = `
    input UpdatePayoutInfoInput {
        accountToken: String!
        bankToken: String!
    }
    type UpdatePayoutInfoPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        payoutAccount: PayoutAccount
    }
    extend type Mutation {
        """Checks if user already has payout info, and if so, assigns the source. Otherwise, creates new payoutInfo"""
        updatePayoutInfo(data: UpdatePayoutInfoInput!): UpdatePayoutInfoPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    updatePayoutInfo(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const payoutAccount = yield context.payments.mutation.upsertUserPayoutInfo(
            {
              data: {
                userId: context.currentUser.id,
                accountToken: args.data.accountToken,
                bankToken: args.data.bankToken,
              },
            },
            context,
            '{id}',
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
            payoutAccount: context.payments.query.payoutAccount(
              {
                where: {
                  id: payoutAccount.id,
                },
              },
              context,
              `{
							id
							type
							stripePayoutInformation {
								id
								accountId
								bank {
									id
									last4
									bankName
								}
							}
                      	}`,
            ),
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            payoutAccount: null,
          }
        }
      })
    },
  },
})
