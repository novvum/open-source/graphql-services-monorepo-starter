'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const approveRejectBooking_1 = require('./approveRejectBooking')
const listCar_1 = require('./listCar')
exports.linkTypeDefs = `
    input RemoveCarPhotoInput {
        carPhotoId: ID!
    }
    input AddCarPhotoInput {
        carId: ID!
        file: Upload!
        isPrimary: Boolean
    }
    type AddRemoveCarPhotoPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        carPhoto: CarPhoto
    }
    input UploadCarDocumentInput {
        carId: ID!
        file: Upload!
        type: CarDocumentTypeEnum!
    }
    type UpdateCarPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        car: Car
    }
    type UploadCarDocumentPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        carDocument: CarDocument
    }
    input UpdateCarDetailsInput {
        carId: ID!
        city: String
        state: String
        street: String
        zip: String
        pickupLat: Float
        pickupLng: Float
        description: String
        vin: String
        makeId: String
        make: String
        modelId: String
        model: String
        yearId: String
        year: String
        mileage: Int
        maxDailyMiles: Int
        dailyPriceInCents: Int
        weeklyPriceInCents: Int
        monthlyPriceInCents: Int
        licensePlate: String
        registrationExpirationAt: DateTime
    }
    input DuplicateCarInput {
        carId: ID!
    }
    extend type Mutation {
        removeCarPhoto(data: RemoveCarPhotoInput!): AddRemoveCarPhotoPayload
        adminRemoveCarPhoto(data: RemoveCarPhotoInput!): AddRemoveCarPhotoPayload
        addCarPhoto(data: AddCarPhotoInput!): AddRemoveCarPhotoPayload
        adminAddCarPhoto(data: AddCarPhotoInput!): AddRemoveCarPhotoPayload
        uploadCarDocument(data: UploadCarDocumentInput!): UploadCarDocumentPayload
        adminUploadCarDocument(data: UploadCarDocumentInput!): UploadCarDocumentPayload
        updateCarDetails(data: UpdateCarDetailsInput!): UpdateCarPayload
        adminUpdateCarDetails(data: UpdateCarDetailsInput!): UpdateCarPayload
        duplicateCar(data: DuplicateCarInput!): UpdateCarPayload
        adminDuplicateCar(data: DuplicateCarInput!): UpdateCarPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    removeCarPhoto(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const carPhotoId = args.data.carPhotoId
          const photoCar = yield context.cars.query.carPhoto(
            {
              where: {
                id: carPhotoId,
              },
            },
            context,
            '{ id car { id } fileId }',
          )
          const carId = photoCar.car.id
          yield verifyCarOwner(carId, context)
          const deletedCarPhoto = yield removeCarPhoto(carPhotoId, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            carPhoto: deletedCarPhoto,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      })
    },
    adminRemoveCarPhoto(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const carPhotoId = args.data.carPhotoId
          const deletedCarPhoto = yield removeCarPhoto(carPhotoId, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            carPhoto: deletedCarPhoto,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      })
    },
    addCarPhoto(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const input = args.data
          yield verifyCarOwner(input.carId, context)
          const createdCarPhoto = yield addCarPhoto(input, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            carPhoto: createdCarPhoto,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      })
    },
    adminAddCarPhoto(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const input = args.data
          const createdCarPhoto = yield addCarPhoto(input, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            carPhoto: createdCarPhoto,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      })
    },
    uploadCarDocument(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const input = args.data
          yield verifyCarOwner(input.carId, context)
          let carDocument = yield uploadCarDocument(input, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            carDocument: carDocument,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.REQUEST_FAILED,
            success: false,
            message: e.message,
          }
        }
      })
    },
    adminUploadCarDocument(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const input = args.data
          let carDocument = yield uploadCarDocument(input, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            carDocument: carDocument,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.REQUEST_FAILED,
            success: false,
            message: e.message,
          }
        }
      })
    },
    updateCarDetails(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const input = args.data
          yield verifyCarOwner(input.carId, context)
          const updatedCar = yield updateCarDetails(input, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            car: updatedCar,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.REQUEST_FAILED,
            success: false,
            message: e.message,
          }
        }
      })
    },
    adminUpdateCarDetails(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const input = args.data
          const updatedCar = yield updateCarDetails(input, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            car: updatedCar,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.REQUEST_FAILED,
            success: false,
            message: e.message,
          }
        }
      })
    },
    duplicateCar(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const input = args.data
          yield verifyCarOwner(input.carId, context)
          const duplicatedCar = yield duplicateCar(input, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            car: duplicatedCar,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.REQUEST_FAILED,
            success: false,
            message: e.message,
          }
        }
      })
    },
    adminDuplicateCar(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const input = args.data
          const duplicatedCar = yield duplicateCar(input, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            car: duplicatedCar,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.REQUEST_FAILED,
            success: false,
            message: e.message,
          }
        }
      })
    },
  },
})
function duplicateCar(input, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const duplicateCarData = yield getDuplicateCarData(input, context)
    const duplicatedCar = context.cars.mutation.createCar(
      {
        data: duplicateCarData,
      },
      context,
      DUPLICATE_CAR_QUERY,
    )
    return duplicatedCar
  })
}
function updateCarDetails(input, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const updateCarData = yield getUpdateCarData(input, context)
    const updatedCar = context.cars.mutation.updateCar(
      {
        where: {
          id: input.carId,
        },
        data: updateCarData,
      },
      context,
    )
    return updatedCar
  })
}
function uploadCarDocument(input, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const car = yield context.cars.query.car(
      {
        where: {
          id: input.carId,
        },
      },
      context,
      '{ id documents { id type fileId } }',
    )
    const existingDocument = car.documents.find(doc => doc.type === input.type)
    const uploadedFile = yield context.files.mutation.upsertFileUpload(
      {
        data: {
          fileId: existingDocument ? existingDocument.fileId : null,
          type: 'CAR_DOCUMENT',
          file: input.file,
        },
      },
      context,
      '{ id path }',
    )
    const upsertCarDocumentData = {
      fileId: uploadedFile.id,
      type: input.type,
      car: {
        connect: {
          id: input.carId,
        },
      },
    }
    let carDocument
    if (existingDocument) {
      carDocument = yield context.cars.mutation.updateCarDocument(
        {
          where: {
            id: existingDocument.id,
          },
          data: upsertCarDocumentData,
        },
        context,
        '{ id }',
      )
    } else {
      carDocument = yield context.cars.mutation.createCarDocument(
        {
          data: upsertCarDocumentData,
        },
        context,
        '{ id }',
      )
    }
    return carDocument
  })
}
function addCarPhoto(input, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const uploadedFile = yield context.files.mutation.uploadFile(
      {
        data: {
          file: input.file,
          type: 'CAR_PHOTO',
        },
      },
      context,
      '{ id }',
    )
    const createdCarPhoto = yield context.cars.mutation.createCarPhoto(
      {
        data: {
          fileId: uploadedFile.id,
          isPrimary: input.isPrimary,
          car: {
            connect: {
              id: input.carId,
            },
          },
        },
      },
      context,
      '{ id }',
    )
    return createdCarPhoto
  })
}
function removeCarPhoto(carPhotoId, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const carPhoto = yield context.cars.query.carPhoto(
      {
        where: {
          id: carPhotoId,
        },
      },
      context,
      '{ id car { id } fileId }',
    )
    yield context.files.mutation.deleteFile(
      {
        where: {
          id: carPhoto.fileId,
        },
      },
      context,
      '{ id }',
    )
    const deletedCarPhoto = yield context.cars.mutation.deleteCarPhoto(
      {
        where: {
          id: carPhoto.id,
        },
      },
      context,
      '{ id }',
    )
    return deletedCarPhoto
  })
}
function verifyCarOwner(carId, context) {
  return __awaiter(this, void 0, void 0, function*() {
    if (!(yield approveRejectBooking_1.isCurrentUserCarOwner(carId, context))) {
      throw lib_1.makeResponseError({
        message: lib_1.NOT_AUTHORIZED_RESPONSE.message,
        code: lib_1.NOT_AUTHORIZED_RESPONSE.code,
      })
    }
  })
}
function pullNonEmptyValues(values, keys) {
  const returnValues = {}
  keys.forEach(key => {
    if (typeof values[key] !== 'undefined') {
      returnValues[key] = values[key]
    }
  })
  return returnValues
}
function getUpdateCarData(input, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const carData = Object.assign(
      {},
      pullNonEmptyValues(input, [
        'description',
        'mileage',
        'maxDailyMiles',
        'dailyPriceInCents',
        'weeklyPriceInCents',
        'monthlyPriceInCents',
        'licensePlate',
        'registrationExpirationAt',
        'vin',
        'makeId',
        'modelId',
        'yearId',
        'make',
        'model',
        'year',
      ]),
    )
    if (input.city) {
      const locationData = {
        city: input.city,
        state: input.state,
        street: input.street,
        formattedAddress: `${input.street}, ${input.city} ${input.state} ${
          input.zip
        }`,
        zip: input.zip,
        lat: input.pickupLat,
        lng: input.pickupLng,
      }
      carData.location = {
        upsert: {
          create: locationData,
          update: locationData,
        },
      }
    }
    if (input.makeId && input.modelId && input.yearId) {
      const makeModelYearData = yield listCar_1.getMakeModelYear(
        {
          makeId: input.makeId,
          modelId: input.modelId,
          yearId: input.yearId,
        },
        context,
      )
      Object.assign(carData, makeModelYearData)
    }
    if (input.make && input.model && input.year) {
      const makeModelYearData = yield listCar_1.getMakeModelYearFromStr(
        {
          make: input.make,
          model: input.model,
          year: input.year,
        },
        context,
      )
      Object.assign(carData, makeModelYearData, {
        make: input.make,
        model: input.model,
        year: input.year,
      })
    }
    return carData
  })
}
exports.getUpdateCarData = getUpdateCarData
function getDuplicateCarData(input, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const car = yield context.cars.query.car(
      {
        where: {
          id: input.carId,
        },
      },
      context,
      DUPLICATE_CAR_QUERY,
    )
    const location = Object.assign({}, car.location)
    const photoFiles = yield Promise.all(
      car.photos.map(photo =>
        context.files.mutation.duplicateFile(
          {
            data: {
              fileId: photo.fileId,
            },
          },
          context,
          '{ id }',
        ),
      ),
    )
    const carData = {
      completedInspection: car.completedInspection,
      dailyPriceInCents: car.dailyPriceInCents,
      description: car.description,
      makeId: car.makeId,
      make: car.make,
      modelId: car.modelId,
      model: car.model,
      yearId: car.yearId,
      year: car.year,
      instantApprove: car.instantApprove,
      licensePlate: car.licensePlate,
      mileage: car.mileage,
      maxDailyMiles: car.maxDailyMiles,
      maxWeeklyMiles: car.maxWeeklyMiles,
      maxMonthlyMiles: car.maxMonthlyMiles,
      monthlyPriceInCents: car.monthlyPriceInCents,
      ownerId: car.ownerId,
      registrationExpirationAt: car.registrationExpirationAt,
      vin: 'xxxx',
      weeklyPriceInCents: car.weeklyPriceInCents,
      status: 'REMOVED',
      verification: 'PENDING_VERIFICATION',
      location: {
        create: car.location,
      },
      photos: {
        create: photoFiles.map((file, index) => ({
          fileId: file.id,
          isPrimary: index === 0,
        })),
      },
    }
    return carData
  })
}
exports.getDuplicateCarData = getDuplicateCarData
const DUPLICATE_CAR_QUERY = `{
    id
	createdAt
	updatedAt
	completedInspection
	dailyPriceInCents
	description
	makeId
	make
	modelId
	model
	yearId
	year
	instantApprove
	licensePlate
	mileage
	maxDailyMiles
	maxWeeklyMiles
	maxMonthlyMiles
	monthlyPriceInCents
	ownerId
	registrationExpirationAt
	vin
	weeklyPriceInCents
	status
	verification
	location {
        lat
        lng
        formattedAddress
        street
        city
        state
        zip
    }
	photos {
        fileId
        isPrimary
    }
}`
