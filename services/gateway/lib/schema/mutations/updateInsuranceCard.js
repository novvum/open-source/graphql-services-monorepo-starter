'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const ABIModel_1 = require('@services/insurance/lib/models/ABIModel')
var moment = require('moment')
exports.linkTypeDefs = `
    input UpdateInsuranceCardInput {
			insuranceId: ID!
			contractID: ID!
    }
    type UpdateInsuranceCardPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        insurance: Insurance
    }
    extend type Mutation {
        updateInsuranceCard(data: UpdateInsuranceCardInput!): UpdateInsuranceCardPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    updateInsuranceCard: (_, args, ctx, info) =>
      __awaiter(this, void 0, void 0, function*() {
        const { insuranceId, contractID } = args.data
        try {
          const abi = new ABIModel_1.default(ctx)
          const newIdCard = yield abi.generateIdCard(contractID)
          const file = yield ctx.files.mutation.uploadFile(
            {
              data: {
                file: yield getInsuranceFileData(
                  newIdCard.content,
                  insuranceId,
                ),
                type: 'RENTAL_INSURANCE',
              },
            },
            ctx,
            `{
					id
					name
					url
					path
					contentType
					type
					secret
				}`,
          )
          // console.log('FILE RESPONSE', file);
          const today = new Date().toISOString()
          const newEndDate = moment(today).add(25, 'd')
          const insuranceWithFile = yield ctx.insurance.mutation.updateInsurance(
            {
              data: {
                insuranceCardImageId: file.id,
                endDateTime: newEndDate,
              },
              where: {
                id: insuranceId,
              },
            },
            ctx,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
            insurance: insuranceWithFile,
          }
        } catch (e) {
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            insurance: null,
          }
        }
      }),
  },
})
function getInsuranceFileData(content, id) {
  return __awaiter(this, void 0, void 0, function*() {
    // console.log('FILEID', id);
    const file = {
      stream: Buffer.from(content, 'base64'),
      filename: id,
      mimetype: 'image/png',
      encoding: null,
    }
    return file
  })
}
