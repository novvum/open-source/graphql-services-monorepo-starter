'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type Rental {
    claim: Claim
  }
  extend type Claim {
    rental: Rental
}
`
exports.resolvers = mergeInfo => ({
  Claim: {
    rental: {
      fragment: `fragment RentalFragment on Claim { rentalId }`,
      resolve(parent, _, context, info) {
        const id = parent.rentalId
        return mergeInfo.delegate(
          'query',
          'rental',
          {
            where: {
              id: id,
            },
          },
          context,
          info,
        )
      },
    },
  },
  Rental: {
    claim: {
      fragment: `fragment ClaimFragment on Rental { id }`,
      resolve(parent, _, context, info) {
        const id = parent.id
        return mergeInfo.delegate(
          'query',
          'claim',
          {
            where: {
              rentalId: id,
            },
          },
          context,
          info,
        )
      },
    },
  },
})
