'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
exports.linkTypeDefs = `
    input AdminSendEmailInput {
      content: String!
      from: String
      subject: String!
      to: String!
      type: EmailTypes
    }
    type AdminSendEmailPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
    }
    extend type Mutation {
        adminSendEmail(data: AdminSendEmailInput!): AdminSendEmailPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    adminSendEmail(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const { content, from, subject, to, type } = args.data
          yield context.notifications.mutation.createLegacyEmailNotification(
            {
              content,
              from: from || 'support@hyrecar.com',
              subject,
              to,
              type: type || `ADMIN_CONTACT`,
            },
            context,
            '{ id }',
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      })
    },
  },
})
