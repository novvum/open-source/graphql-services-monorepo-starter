'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const express = require('express')
const publicServer_1 = require('./publicServer')
const internalServer_1 = require('./internalServer')
const config_1 = require('./config')
const apolloEngine_1 = require('./utils/apolloEngine')
const serverConfig = {
  port: config_1.default.PORT,
  uploads: {
    maxFiles: 10,
  },
  playground: '/',
}
switch (config_1.default.SERVER_TYPE) {
  case 'api':
    publicServer_1.default(serverConfig)
    break
  case 'internal':
    internalServer_1.default(serverConfig)
    break
  default:
    const server = express()
    const publicPort = config_1.default.PORT + 2
    const pServer = publicServer_1.default(
      Object.assign({}, serverConfig, { port: publicPort }),
      '/api',
    )
    const internalPort = config_1.default.PORT + 1
    const iServer = internalServer_1.default(
      Object.assign({}, serverConfig, { port: internalPort }),
      '/internal',
    )
    server.use('/api', pServer.express)
    server.use('/internal', iServer.express)
    apolloEngine_1.startApolloEngine({
      port: config_1.default.PORT,
      paths: ['/api', '/internal'],
      expressApp: server,
      serverType: 'api',
    })
}
