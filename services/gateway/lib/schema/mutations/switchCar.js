'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const utils_1 = require('./utils')
const RentalOverview_1 = require('../RentalOverview')
const RentalContractsModel_1 = require('../../models/RentalContractsModel')
exports.linkTypeDefs = `
    input SwitchCarInput {
        rentalId: ID!
        switchToCarId: ID!
    }
    type SwitchRentalCarPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        rental: Rental
    }
    extend type Mutation {
        switchCar(where: SwitchCarInput!): SwitchRentalCarPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    switchCar(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const rental = yield context.rentals.query.rental(
            {
              where: {
                id: args.where.rentalId,
              },
            },
            context,
            '{ id status driverId disallowExtensions carId rentalPeriods { id } }',
          )
          yield validateRequest(
            { rental, newCarId: args.where.switchToCarId },
            context,
          )
          const switchedRental = yield switchCar(
            { input: args.where, rental },
            context,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            rental: switchedRental,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            rating: null,
          }
        }
      })
    },
  },
})
function validateRequest(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { rental, newCarId } = args
    if (rental.carId === newCarId) {
      throw lib_1.makeResponseError({
        message: `Cannot switch to the same car. Original Car ID: ${
          rental.carId
        }, new Car ID: ${newCarId}`,
        code: lib_1.ResponseCodes.BAD_REQUEST,
      })
    }
    const SWITCHABLE_STATUSES = [
      'PENDING_INSURANCE',
      'PENDING_PICKUP',
      'ACTIVE',
    ]
    if (!SWITCHABLE_STATUSES.includes(rental.status)) {
      throw lib_1.makeResponseError({
        message: `A rental with the status '${
          rental.status
        }' cannot be switched.`,
        code: lib_1.ResponseCodes.BAD_REQUEST,
      })
    }
  })
}
function switchCar(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { input, rental } = args
    switch (rental.status) {
      case 'PENDING_INSURANCE':
        return yield switchCarId(
          { rental, newCarId: input.switchToCarId },
          context,
        )
      case 'PENDING_PICKUP':
      case 'ACTIVE':
        return yield switchInsuredCar(
          { rental, newCarId: input.switchToCarId },
          context,
        )
      default:
        return null
    }
  })
}
function switchCarId(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { rental, newCarId } = args
    const updatedRental = yield context.rentals.mutation.switchRentalCar(
      {
        where: {
          id: rental.id,
        },
        data: {
          newCarId: newCarId,
        },
      },
      context,
      '{ id carId status currentRentalPeriod { id numDays startDate endDate } }',
    )
    const newRentalOverview = yield RentalOverview_1.getRentalOverview(
      rental.id,
      context,
    )
    const switchedPaymentInfo = yield context.payments.mutation.switchCarPayment(
      {
        data: {
          rentalPeriodId: updatedRental.currentRentalPeriod.id,
        },
      },
      context,
      '{ id }',
    )
    yield Promise.all([
      context.rentals.mutation.updateRentalPeriod(
        {
          where: {
            id: updatedRental.currentRentalPeriod.id,
          },
          data: {
            paymentId: switchedPaymentInfo.id,
          },
        },
        context,
      ),
      rentCar(updatedRental.carId, context),
      relistCar(rental.carId, context),
    ])
    const contractsModel = new RentalContractsModel_1.default(context)
    contractsModel.uploadSwitchedRentalPeriodContract(
      {
        driverId: rental.driverId,
        rentalPeriodDetails: {
          carId: updatedRental.carId,
          id: updatedRental.currentRentalPeriod.id,
          numDays: updatedRental.currentRentalPeriod.numDays,
          startDate: updatedRental.currentRentalPeriod.startDate,
          totalPriceInCents: Math.round(
            newRentalOverview.totalCostInCents *
              (updatedRental.currentRentalPeriod.numDays /
                newRentalOverview.totalDays),
          ),
          rentalId: rental.id,
        },
      },
      context,
    )
    return updatedRental
  })
}
function relistCar(releasedCarId, context) {
  return context.cars.mutation.addCarToMarket(
    {
      data: {
        carId: releasedCarId,
      },
    },
    context,
    '{ id }',
  )
}
exports.relistCar = relistCar
function rentCar(rentedCarId, context) {
  return context.cars.mutation.updateCar(
    {
      where: {
        id: rentedCarId,
      },
      data: {
        status: 'RENTED',
      },
    },
    context,
    '{ id }',
  )
}
function switchInsuredCar(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { rental, newCarId } = args
    yield closeInsurance(rental, context)
    return yield switchCarId({ rental, newCarId }, context)
  })
}
function closeInsurance(rental, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const insurance = yield utils_1.getCurrentRentalInsurance(rental, context)
    const alreadyClosed = ['CANCELED', 'CLOSED'].includes(insurance.status)
    if (alreadyClosed) {
      return insurance
    }
    const insuranceStarted = new Date(insurance.startDateTime) < new Date()
    if (insuranceStarted) {
      yield context.insurance.mutation.closeInsurance(
        {
          insuranceId: insurance.id,
          isDamaged: false,
        },
        context,
        '{ id }',
      )
    } else {
      yield context.insurance.mutation.cancelInsurance(
        {
          insuranceId: insurance.id,
        },
        context,
        '{ id }',
      )
    }
  })
}
exports.closeInsurance = closeInsurance
