import { MergedContext } from '../getContext'

export const linkTypeDefs = `
    extend type Query {
        hello: Boolean
    }
`
export const resolvers: any = {
  Query: {
    hello: {
      fragment: `fragment FileFragment on Query { getSampleQuery }`,
      async resolve(parent, _, context: MergedContext, info) {
        return false
      },
    },
  },
}
