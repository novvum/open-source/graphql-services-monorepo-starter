'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.makeResponseError = args => {
  const e = new Error(args.message)
  // @ts-ignore
  e.code = args.code
  return e
}
//# sourceMappingURL=ResponseError.js.map
