'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const getContext_1 = require('../../getContext')
const utils_1 = require('./utils')
var moment = require('moment')
exports.linkTypeDefs = `
    input GenerateInsuranceInput {
		rentalId: ID!
		contractId: String
		vin: String
		carYear: String
		carMake: String
		carModel: String
		carState: String
		carOwnerName: String
		carOwnerAddressLine1: String
		carOwnerAddressLine2: String
		carOwnerCity: String
		carOwnerState: String
		carOwnerZIP: String
		carOwnerPhone: String
		startDateTime: DateTime		
		driverFirstName: String
		driverLastName: String
		driverDOB: String
		driverExpiration: String
		driverLicenseNumber: String
		driverLicenseState: String
		driverAddressLine1: String
		driverAddressLine2: String
		driverCity: String
		driverState: String
		driverZIP: String
		driverPhone: String		
	}
	input UpdateInsuranceInput {
		rentalId: ID!
		contractId: String!
	}
    type GenerateInsurancePayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        insurance: Insurance
    }
    extend type Mutation {
		generateInsurance(data: GenerateInsuranceInput!): GenerateInsurancePayload
		updateRentalInsurance(data: UpdateInsuranceInput!): GenerateInsurancePayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    updateRentalInsurance: (_, args, ctx, info) =>
      __awaiter(this, void 0, void 0, function*() {
        try {
          const rentalId = args.data.rentalId
          const existingInsurances = yield ctx.insurance.query.insurances(
            {
              where: {
                rentalId: rentalId,
              },
              first: 1,
              orderBy: 'createdAt_DESC',
            },
            ctx,
            '{ id status }',
          )
          const insuranceWithFile = yield generateInsurance(args.data, ctx)
          if (existingInsurances.length > 0) {
            yield ctx.insurance.mutation.updateInsurance(
              {
                where: {
                  id: existingInsurances[0].id,
                },
                data: {
                  status: 'CLOSED',
                },
              },
              getContext_1.context,
              '{ id }',
            )
          }
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
            insurance: insuranceWithFile,
          }
        } catch (e) {
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      }),
    generateInsurance: (_, args, ctx, info) =>
      __awaiter(this, void 0, void 0, function*() {
        try {
          const input = args.data
          const insuranceWithFile = yield generateInsurance(input, ctx)
          const rental = yield ctx.rentals.query.rental(
            {
              where: {
                id: input.rentalId,
              },
            },
            ctx,
            '{ id currentRentalPeriod { id numDays } }',
          )
          const startDate = new Date(input.startDateTime).toISOString()
          const updatedRental = yield ctx.rentals.mutation.updateRental(
            {
              data: {
                status: 'PENDING_PICKUP',
                rentalPeriods: {
                  update: {
                    where: {
                      id: rental.currentRentalPeriod.id,
                    },
                    data: {
                      startDate: startDate,
                      endDate: require('moment')(new Date(startDate))
                        .add(rental.currentRentalPeriod.numDays, 'days')
                        .toDate()
                        .toISOString(),
                    },
                  },
                },
              },
              where: {
                id: input.rentalId,
              },
            },
            getContext_1.context,
          )
          if (input.driverExpiration) {
            updateDriverLicenseExpiration(
              {
                driverId: updatedRental.driverId,
                licenseExpirationDate: input.driverExpiration,
              },
              getContext_1.context,
            )
          }
          generateContract(
            {
              driverId: updatedRental.driverId,
              rental: updatedRental,
            },
            getContext_1.context,
          )
          yield captureRentalCharge(input)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
            insurance: insuranceWithFile,
          }
        } catch (e) {
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            paymentAccount: null,
          }
        }
      }),
  },
})
function generateInsurance(input, ctx) {
  return __awaiter(this, void 0, void 0, function*() {
    const insuranceInput = yield mapInsurance(input, ctx)
    const contractData = {
      data: insuranceInput,
      contract: null,
    }
    if (!contractData.data.contractId) {
      const vehicle = yield mapVehicle(input)
      const driver = yield mapDriver(input)
      const contractDetails = yield mapContractDetails(input)
      contractData.contract = {
        vehicle,
        driver,
        contractDetails,
      }
    }
    const insuranceWithFile = yield startInsurance(contractData, ctx)
    return insuranceWithFile
  })
}
function startInsurance(contractData, ctx) {
  return __awaiter(this, void 0, void 0, function*() {
    const payload = yield ctx.insurance.mutation.startInsurance(
      contractData,
      ctx,
      `{
		insurance {
			id
			rentalId
		}
		idCard {
			content
			fileType
		}
	}`,
    )
    // console.log('PAYLOAD', payload);
    const file = yield ctx.files.mutation.uploadFile(
      {
        data: {
          file: yield getInsuranceFileData(
            payload.idCard.content,
            payload.insurance.id,
          ),
          type: 'RENTAL_INSURANCE',
        },
      },
      ctx,
      `{
		id
		name
		url
		path
		contentType
		type
		secret
	}`,
    )
    // console.log('FILE RESPONSE', file);
    const insuranceWithFile = yield ctx.insurance.mutation.updateInsurance(
      {
        data: {
          insuranceCardImageId: file.id,
        },
        where: {
          id: payload.insurance.id,
        },
      },
      ctx,
    )
    return insuranceWithFile
  })
}
function generateContract(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const rental = yield context.rentals.query.rental(
      {
        where: {
          id: args.rental.id,
        },
      },
      context,
      '{ id currentRentalPeriod { id carId paymentId numDays startDate } }',
    )
    const latestRentalPeriod = rental.currentRentalPeriod
    yield utils_1.generateContractFromRentalPeriod(
      {
        driverId: args.driverId,
        rentalPeriod: latestRentalPeriod,
      },
      context,
    )
  })
}
function updateDriverLicenseExpiration(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    return yield context.users.mutation.updateDriver(
      {
        where: {
          id: args.driverId,
        },
        data: {
          legalInfo: {
            update: {
              licenseExpirationDate: new Date(
                args.licenseExpirationDate,
              ).toISOString(),
            },
          },
        },
      },
      context,
      '{ id }',
    )
  })
}
function getInsuranceFileData(content, id) {
  return __awaiter(this, void 0, void 0, function*() {
    // console.log('FILEID', id);
    const file = {
      stream: Buffer.from(content, 'base64'),
      filename: id,
      mimetype: 'image/png',
      encoding: null,
    }
    return file
  })
}
function abiDateFormat(dateObj) {
  return __awaiter(this, void 0, void 0, function*() {
    const date = yield moment(dateObj).format('MM/DD/YYYY')
    return date
  })
}
function mapVehicle(data) {
  const vehicleOwner = {
    vehicleOwnerName: data.carOwnerName,
    vehicleOwnerAddressLine1: data.carOwnerAddressLine1,
    vehicleOwnerAddressLine2: data.carOwnerAddressLine2,
    vehicleOwnerCity: data.carOwnerCity,
    vehicleOwnerState: data.carOwnerState,
    vehicleOwnerZIP: data.carOwnerZIP,
    vehicleOwnerPhone: normalizePhoneNumber(data.carOwnerPhone),
  }
  const vehicle = {
    VIN: data.vin,
    vehicleYear: data.carYear,
    vehicleMake: data.carMake,
    vehicleModel: data.carModel,
    vehicleState: data.carState,
    vehicleOwner,
  }
  return vehicle
}
function mapDriver(data) {
  return __awaiter(this, void 0, void 0, function*() {
    const expDate = new Date(data.driverExpiration)
    const driver = {
      driverFirstName: data.driverFirstName,
      driverLastName: data.driverLastName,
      driverDOB: yield abiDateFormat(data.driverDOB),
      driverExpiration: yield abiDateFormat(expDate),
      driverLicenseNumber: data.driverLicenseNumber,
      driverLicenseState: data.driverLicenseState,
      driverAddressLine1: data.driverAddressLine1,
      driverAddressLine2: data.driverAddressLine2,
      driverCity: data.driverCity,
      driverState: data.driverState,
      driverZIP: data.driverZIP,
      driverPhone: normalizePhoneNumber(data.driverPhone),
    }
    return driver
  })
}
function mapContractDetails(data) {
  return __awaiter(this, void 0, void 0, function*() {
    const startDateTime = new Date(data.startDateTime)
    var momentTz = require('moment-timezone')
    const pacificDate = momentTz(new Date(startDateTime)).tz(
      'America/Los_Angeles',
    )
    const startDate = yield abiDateFormat(pacificDate.toDate())
    const Hour = yield pacificDate.format('HH')
    const Minute = yield pacificDate.format('mm')
    const Time = {
      Hour,
      Minute,
    }
    const contractDetails = {
      startDate,
      Time,
    }
    console.log(contractDetails)
    return contractDetails
  })
}
function mapInsurance(data, ctx) {
  return __awaiter(this, void 0, void 0, function*() {
    const rental = yield ctx.rentals.query.rental(
      {
        where: {
          id: data.rentalId,
        },
      },
      ctx,
      '{ id carId }',
    )
    const insurance = {
      rentalId: data.rentalId,
      carId: rental.carId,
      vin: data.vin,
      startDateTime: data.startDateTime,
      contractId: data.contractId,
    }
    return insurance
  })
}
function captureRentalCharge(args) {
  return __awaiter(this, void 0, void 0, function*() {
    const { rentalId } = args
    const rentalPeriod = (yield getContext_1.context.rentals.query.rental(
      {
        where: {
          id: rentalId,
        },
      },
      getContext_1.context,
      '{ id currentRentalPeriod { id } }',
    )).currentRentalPeriod
    try {
      const charge = getContext_1.context.payments.mutation.captureBookingPayment(
        {
          data: {
            rentalId: rentalId,
            rentalPeriodId: rentalPeriod.id,
          },
        },
        getContext_1.context,
      )
      return charge
    } catch (error) {
      console.error(error)
      throw lib_1.makeResponseError({
        message: error.message,
        code: lib_1.ResponseCodes.REQUEST_FAILED,
      })
    }
  })
}
function normalizePhoneNumber(phoneNumber) {
  return (phoneNumber || '').replace(/-/g, '')
}
