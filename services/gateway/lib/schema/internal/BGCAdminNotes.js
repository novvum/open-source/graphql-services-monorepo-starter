'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type BackgroundCheck {
    notes(where: AdminNoteWhereInput, orderBy: AdminNoteOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): AdminNoteConnection
  }
`
exports.resolvers = mergeInfo => ({
  BackgroundCheck: {
    notes: {
      fragment: `fragment AdminNoteFragment on BackgroundCheck { id }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.id
          // TODO: get list of owner's car id's for OR condition below
          const notes = yield context.admin.query.notesConnection(
            Object.assign({}, args, {
              where: Object.assign({ backgroundCheckId: id }, args.where),
            }),
            context,
            info,
          )
          if (notes.edges.length == undefined) {
            return null
          }
          return notes
        })
      },
    },
  },
})
