'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const RentalContractsModel_1 = require('../models/RentalContractsModel')
exports.linkTypeDefs = `
    input ProspectiveRentalContractInput {
        carId: ID!
        rentalDays: Int!
        startDate: DateTime!
        couponCode: String
    }
    extend type Query {
      getProspectiveRentalContractUrl(where: ProspectiveRentalContractInput): String!
    }
    extend type Rental {
        prospectiveRentalContractUrl: String
    }
`
exports.resolvers = mergeInfo => ({
  Query: {
    getProspectiveRentalContractUrl(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        const input = Object.assign({}, args.where, {
          driverId: context.currentUser.driver.id,
        })
        const url = yield getProspectiveRentalContractUrl(input, context)
        return url
      })
    },
  },
  Rental: {
    prospectiveRentalContractUrl: {
      fragment: `fragment ProspectiveRentalContractFragment on Rental {
                id
            }`,
      resolve(rental, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const parent = yield context.rentals.query.rental(
            {
              where: {
                id: rental.id,
              },
            },
            context,
            `{
                    id
                    carId
                    driverId
                    requestedPickupAt
                    rentalPeriods(first: 1, orderBy: endDate_DESC) {
                        id
                        startDate
                        numDays
                        paymentId
                    }
                }`,
          )
          const url = yield getOwnerProspectiveRentalContractUrl(
            parent,
            context,
          )
          return url
        })
      },
    },
  },
})
function getProspectiveRentalContractUrl(input, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const contractsModel = new RentalContractsModel_1.default(context)
    const {
      car,
      driver,
      owner,
    } = yield contractsModel.getGeneralContractDetails({
      carId: input.carId,
      driverId: input.driverId,
    })
    const application = yield contractsModel.getProspectiveApplicationDetails(
      input,
      car,
    )
    const url = contractsModel.generateContractUrl({
      driver,
      car,
      application,
      owner,
    })
    return url
  })
}
function getOwnerProspectiveRentalContractUrl(rental, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const contractsModel = new RentalContractsModel_1.default(context)
    const {
      car,
      driver,
      owner,
    } = yield contractsModel.getGeneralContractDetails({
      driverId: rental.driverId,
      carId: rental.carId,
      rentalId: rental.id,
    })
    const rentalPeriod = {
      paymentId: rental.rentalPeriods[0].paymentId,
      numDays: rental.rentalPeriods[0].numDays,
      startDate: rental.rentalPeriods[0].startDate,
    }
    const application = yield contractsModel.getRentalPeriodContractDetails(
      rentalPeriod,
    )
    const url = contractsModel.generateContractUrl({
      owner,
      driver,
      car,
      application,
    })
    return url
  })
}
