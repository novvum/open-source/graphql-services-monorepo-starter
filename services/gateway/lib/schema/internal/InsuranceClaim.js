'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type Insurance {
    claim: Claim
  }
  extend type Claim { 
    insurance: Insurance
}
`
exports.resolvers = mergeInfo => ({
  Claim: {
    insurance: {
      fragment: `fragment InsuranceFragment on Claim { insuranceId }`,
      resolve(parent, _, context, info) {
        const id = parent.insuranceId
        return mergeInfo.delegate(
          'query',
          'insurance',
          {
            where: {
              id: id,
            },
          },
          context,
          info,
        )
      },
    },
  },
  Insurance: {
    claim: {
      fragment: `fragment ClaimFragment on Insurance { id }`,
      resolve(parent, _, context, info) {
        const id = parent.id
        return mergeInfo.delegate(
          'query',
          'claim',
          {
            where: {
              insuranceId: id,
            },
          },
          context,
          info,
        )
      },
    },
  },
})
