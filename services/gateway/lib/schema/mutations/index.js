'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const requestBooking = require('./requestBooking')
const updateProfile = require('./updateProfile')
const updatePaymentInfo = require('./updatePaymentInfo')
const updatePayoutInfo = require('./updatePayoutInfo')
const startBackgroundCheck = require('./startBackgroundCheck')
const approveRejectBooking = require('./approveRejectBooking')
const toggleRentalExtension = require('./toggleRentalExtension')
const confirmDropoff = require('./confirmDropoff')
const confirmPickup = require('./confirmPickup')
const rateDriver = require('./rateDriver')
const generateInsurance = require('./generateInsurance')
const toggleListing = require('./toggleListing')
const listCar = require('./listCar')
const manageCar = require('./manageCar')
const adminApproveRejectCar = require('./adminApproveRejectCar')
const adminCancelRental = require('./adminCancelRental')
const extendRental = require('./extendRental')
const switchCar = require('./switchCar')
const adminToggleBlockDriverBooking = require('./adminToggleBlockDriverBooking')
const adminApproveRejectDriverBgc = require('./adminApproveRejectDriverBgc')
const updateInsuranceCard = require('./updateInsuranceCard')
const adminSendEmail = require('./adminSendEmail')
const adminCreateClaim = require('./adminCreateClaim')
const adminModifyClaim = require('./adminModifyClaim')
exports.default = {
  schemas: [
    requestBooking.linkTypeDefs,
    updateProfile.linkTypeDefs,
    updatePaymentInfo.linkTypeDefs,
    updatePayoutInfo.linkTypeDefs,
    startBackgroundCheck.linkTypeDefs,
    generateInsurance.linkTypeDefs,
    approveRejectBooking.linkTypeDefs,
    toggleRentalExtension.linkTypeDefs,
    confirmDropoff.linkTypeDefs,
    confirmPickup.linkTypeDefs,
    rateDriver.linkTypeDefs,
    toggleListing.linkTypeDefs,
    listCar.linkTypeDefs,
    manageCar.linkTypeDefs,
    adminApproveRejectCar.linkTypeDefs,
    adminCancelRental.linkTypeDefs,
    extendRental.linkTypeDefs,
    switchCar.linkTypeDefs,
    adminToggleBlockDriverBooking.linkTypeDefs,
    adminApproveRejectDriverBgc.linkTypeDefs,
    updateInsuranceCard.linkTypeDefs,
    adminSendEmail.linkTypeDefs,
    adminCreateClaim.linkTypeDefs,
    adminModifyClaim.linkTypeDefs,
  ],
  resolvers: [
    requestBooking.resolvers,
    updateProfile.resolvers,
    updatePaymentInfo.resolvers,
    updatePayoutInfo.resolvers,
    startBackgroundCheck.resolvers,
    generateInsurance.resolvers,
    approveRejectBooking.resolvers,
    toggleRentalExtension.resolvers,
    confirmDropoff.resolvers,
    confirmPickup.resolvers,
    rateDriver.resolvers,
    toggleListing.resolvers,
    listCar.resolvers,
    manageCar.resolvers,
    adminApproveRejectCar.resolvers,
    adminCancelRental.resolvers,
    extendRental.resolvers,
    switchCar.resolvers,
    adminToggleBlockDriverBooking.resolvers,
    adminApproveRejectDriverBgc.resolvers,
    updateInsuranceCard.resolvers,
    adminSendEmail.resolvers,
    adminCreateClaim.resolvers,
    adminModifyClaim.resolvers,
  ],
}
