'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
function oChainArray(data) {
  return data.map(d => lib_1.optionalChaining(() => d) || null)
}
exports.oChainArray = oChainArray
function oChainObject(data) {
  const keys = Object.keys(data)
  const oChainedResults = keys.map(
    k => lib_1.optionalChaining(() => data[k]) || null,
  )
  return oChainedResults
}
exports.oChainObject = oChainObject
function mapLocation(args) {
  const street = lib_1.optionalChaining(() => args.street) || ''
  const city = lib_1.optionalChaining(() => args.city) || ''
  const state = lib_1.optionalChaining(() => args.state) || ''
  const zip = lib_1.optionalChaining(() => args.zip) || ''
  return `
  ${street}
  ${city}
  ${state}
  ${zip}`
}
exports.mapLocation = mapLocation
