'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
    extend type CarDocument {
        file: File
    }
    extend type CarPhoto {
        file: File
    }
`
exports.resolvers = mergeInfo => ({
  CarPhoto: {
    file: {
      fragment: `fragment FileFragment on CarPhoto { fileId }`,
      resolve(parent, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.fileId
          if (!id) {
            return null
          }
          return yield context.files.query.file(
            {
              where: {
                id: id,
              },
            },
            context,
            info,
          )
        })
      },
    },
  },
  CarDocument: {
    file: {
      fragment: `fragment FileFragment on CarDocument { id fileId }`,
      resolve(parent, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.fileId
          if (!id) {
            return null
          }
          return yield context.files.query.file(
            {
              where: {
                id: id,
              },
            },
            context,
            info,
          )
        })
      },
    },
  },
})
