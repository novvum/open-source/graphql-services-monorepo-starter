export declare type ApiResponse = Promise<any>
export interface GetFunc {
  (uri: string, queryParams?: string): ApiResponse
}
export interface ApiBody {
  body?: any
  headers?: any
}
export interface PostOpts extends ApiBody {}
export interface PostFunc {
  (uri: string, opts: PostOpts): ApiResponse
}
export interface PutOpts extends ApiBody {}
export interface PutFunc {
  (uri: string, opts?: PutOpts): ApiResponse
}
export interface PatchFunc {
  (uri: string): ApiResponse
}
export interface SetTokenFunc {
  (token: string): any
}
export interface IAPI {
  baseUri: string
  token: string
  apiCaller: any
  get: GetFunc
  post: PostFunc
  put: PutFunc
  patch: PatchFunc
  setToken: SetTokenFunc
}
export declare class LegacyApi implements IAPI {
  static SERVER_DATE_FORMAT: string
  baseUri: string
  token: string
  apiCaller: any
  constructor(args: { token: string; baseUri: string })
  setToken(newToken: any): void
  getBaseUri(): string
  get(uri: any, queryParams?: string): ApiResponse
  post(uri: string, opts?: PostOpts): ApiResponse
  put(uri: string, opts?: PutOpts): ApiResponse
  patch(uri: string): any
}
