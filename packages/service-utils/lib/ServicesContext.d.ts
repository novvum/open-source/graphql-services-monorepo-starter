/// <reference types="express" />
import * as express from 'express'
import { LegacyApi } from './legacyApi'
export declare type SlsSDKType = {
  emit: ({ event: string, data: Object }) => Promise<Response>
  invoke: ({ functionId: string, data: Object }) => Promise<Response>
}
export declare type Request = express.Request
export declare type Response = express.Response
export declare type ServicesContext = {
  request: Request
  legacyApi?: LegacyApi
  currentUser: {
    id: any
    driver?: {
      id: any
    }
    owner?: {
      id: any
    }
    admin?: {
      id: any
    }
    roles?: string[]
    status?: 'ACTIVE' | 'BLOCKED'
  }
  eventGateway: SlsSDKType
  [key: string]: any
}
