'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type Driver {
    claims(filter: ClaimWhereInput, orderBy: ClaimOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Claim!],
    claimsConnection(filter: ClaimWhereInput, orderBy: ClaimOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): ClaimConnection
    driverClaimsMeta: DriverClaimsMeta
  }
  extend type Claim {
    driver: Driver
}
`
exports.resolvers = mergeInfo => ({
  Claim: {
    driver: {
      fragment: `fragment DriverFragment on Claim { driverId }`,
      resolve(parent, _, context, info) {
        const id = parent.driverId
        return mergeInfo.delegate(
          'query',
          'driver',
          {
            where: {
              id: id,
            },
          },
          context,
          info,
        )
      },
    },
  },
  Driver: {
    claims: {
      fragment: `fragment ClaimFragment on Driver { id }`,
      resolve(parent, args, context, info) {
        const id = parent.id
        return mergeInfo.delegate(
          'query',
          'claims',
          Object.assign({}, args, {
            where: {
              driverId: id,
            },
          }),
          context,
          info,
        )
      },
    },
    claimsConnection: {
      fragment: `fragment ClaimConnectionFragment on Driver { id }`,
      resolve(parent, args, context, info) {
        const id = parent.id
        return mergeInfo.delegate(
          'query',
          'claimsConnection',
          Object.assign({}, args, {
            where: {
              driverId: id,
            },
          }),
          context,
          info,
        )
      },
    },
    driverClaimsMeta: {
      fragment: `fragment ClaimFragment on Driver { id }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.id
          if (!id) {
            return null
          }
          return context.admin.query.driverClaimsMeta(
            {
              driverId: id,
            },
            context,
            info,
          )
        })
      },
    },
  },
})
