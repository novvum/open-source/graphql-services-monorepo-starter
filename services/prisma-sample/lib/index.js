'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const graphql_tools_1 = require('graphql-tools')
const prisma_1 = require('./generated/prisma')
const resolvers_1 = require('./resolvers')
const app_1 = require('./generated/app')
const typeDefs = require('./generated/app.graphql')
class PrismaSampleBinding extends app_1.Binding {
  constructor(args) {
    const { prismaEndpoint, prismaSecret, debug = true } = args
    const schema = graphql_tools_1.makeExecutableSchema({
      typeDefs,
      resolvers: resolvers_1.resolvers,
      resolverValidationOptions: {
        requireResolversForResolveType: false,
      },
    })
    super({
      schema,
    })
    this.schema = schema
    this.db = new prisma_1.Prisma({
      endpoint: prismaEndpoint,
      secret: prismaSecret,
      debug,
    })
  }
}
exports.PrismaSampleBinding = PrismaSampleBinding
//# sourceMappingURL=index.js.map
