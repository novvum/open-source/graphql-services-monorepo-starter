import { ApolloEngine } from 'apollo-engine'
import { Server as HttpServer } from 'http'
import config from '../config'
import { GraphQLServer, OptionsWithoutHttps, Options } from 'graphql-yoga'

export const startApolloEngine = (args: {
  port: string | number
  serverType: string
  paths?: Array<string>
  server?: GraphQLServer
  expressApp?
}) => {
  const { port, serverType, paths, server, expressApp } = args
  const express = expressApp || server.express
  if (config.disableEngine) {
    return express.listen(port, () =>
      console.log(
        `${serverType} server is running on http://localhost:${port}`,
      ),
    )
  }
  const engine = new ApolloEngine({
    apiKey: config.apolloEngine[serverType],
    frontends: [
      {
        overrideGraphqlResponseHeaders: {
          'Access-Control-Allow-Origin': '*',
        },
      },
    ],
  })
  return engine.listen(
    {
      port,
      expressApp: express,
      graphqlPaths: paths || ['/'],
    },
    () =>
      console.log(
        `${serverType} server with Apollo Engine is running on http://localhost:${port}`,
      ),
  )
}
