'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const apollo_engine_1 = require('apollo-engine')
const config_1 = require('../config')
exports.startApolloEngine = args => {
  const { port, serverType, paths, server, expressApp } = args
  const engine = new apollo_engine_1.ApolloEngine({
    apiKey: config_1.default.apolloEngine[serverType],
  })
  return engine.listen(
    {
      port,
      expressApp: expressApp || server.express,
      graphqlPaths: paths || ['/'],
    },
    () =>
      console.log(
        `${serverType} server with Apollo Engine is running on http://localhost:${port}`,
      ),
  )
}
