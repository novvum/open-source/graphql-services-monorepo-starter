'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const usersMocks_1 = require('./usersMocks')
const objectAssignDeep = require(`object-assign-deep`)
function randomDate(start, end) {
  return new Date(
    start.getTime() + Math.random() * (end.getTime() - start.getTime()),
  )
}
exports.scalarMocks = {
  DateTime: () => randomDate(new Date(2012, 0, 1), new Date()),
}
exports.mocks = objectAssignDeep({}, exports.scalarMocks, usersMocks_1.default)
