'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const config_1 = require('../../config')
exports.linkTypeDefs = `
    input UpdateProfileInput {
        # owner
        photo: Upload
        # owner
        firstName: String
        # owner
        lastName: String
        # both
        phone: String
        # owner
        zip: String
        # owner
        address: String
        # both
        tncAccepted: Boolean
        # driver
        about: String
        # driver
        apt: String
        hNo: String
        # driver
        street: String
        # driver
        city: String
        # driver
        state: String
    }
    type UpdateProfilePayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        user: User
    }
    input UpdateProfilePhotoInput {
        file: Upload!
    }
    type UpdateProfilePhotoPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        file: File
        user: User
    }
    input AdminUpdateUserWhereInput {
        userId: ID!
    }
    extend type Mutation {
        updateProfilePhoto(data: UpdateProfilePhotoInput!): UpdateProfilePhotoPayload
        adminUpdateProfilePhoto(data: UpdateProfilePhotoInput!, where: AdminUpdateUserWhereInput!): UpdateProfilePhotoPayload
        updateProfile(input: UpdateProfileInput): UpdateProfilePayload
        adminUpdateProfile(input: UpdateProfileInput!, where: AdminUpdateUserWhereInput!): UpdateProfilePayload
        uploadDriversLicense(photo: Upload!): UpdateProfilePayload
        adminUploadDriversLicense(photo: Upload!, where: AdminUpdateUserWhereInput!): UpdateProfilePayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    updateProfilePhoto: (_, args, context, info) =>
      __awaiter(this, void 0, void 0, function*() {
        const userDetails = yield getUserDetails(
          context.currentUser.id,
          context,
        )
        const { file, user } = yield updateProfilePhoto(
          { photo: args.data.file, user: userDetails },
          context,
        )
        return {
          code: lib_1.ResponseCodes.OK,
          success: true,
          file: file,
          user: Object.assign({}, user, { profilePhoto: file }),
        }
      }),
    adminUpdateProfilePhoto: (_, args, context, info) =>
      __awaiter(this, void 0, void 0, function*() {
        const userDetails = yield getUserDetails(args.where.userId, context)
        const { file, user } = yield updateProfilePhoto(
          { photo: args.data.file, user: userDetails },
          context,
        )
        return {
          code: lib_1.ResponseCodes.OK,
          success: true,
          file: file,
          user: Object.assign({}, user, { profilePhoto: file }),
        }
      }),
    updateProfile: (_, args, context) =>
      __awaiter(this, void 0, void 0, function*() {
        try {
          verifyCurrentUser(context)
          const userId = context.currentUser.id
          const input = args.input
          const updatedUser = yield updateUserProfile(
            { userId, input },
            context,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            user: updatedUser,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      }),
    adminUpdateProfile: (_, args, context) =>
      __awaiter(this, void 0, void 0, function*() {
        try {
          const input = args.input
          const updatedUser = yield updateUserProfile(
            { userId: args.where.userId, input },
            context,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            user: updatedUser,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      }),
    uploadDriversLicense: (_, args, context) =>
      __awaiter(this, void 0, void 0, function*() {
        try {
          const userId = context.currentUser.id
          verifyCurrentUser(context)
          const input = args
          const { file, user } = yield uploadDriversLicense(
            { input, userId },
            context,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            file: file,
            user: user,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      }),
    adminUploadDriversLicense: (_, args, context) =>
      __awaiter(this, void 0, void 0, function*() {
        try {
          const userId = args.where.userId
          const input = args
          const { file, user } = yield uploadDriversLicense(
            { input, userId },
            context,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            file: file,
            user: user,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      }),
  },
})
function uploadDriversLicense(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { input, userId } = args
    const file = yield context.files.mutation.uploadFile(
      {
        data: {
          file: input.photo,
          type: 'LICENSE_PHOTO',
        },
      },
      context,
      '{ id path url }',
    )
    const user = yield context.users.mutation.updateUser(
      {
        where: {
          id: userId,
        },
        data: {
          driver: {
            update: {
              legalInfo: {
                upsert: {
                  update: {
                    licensePhotoId: file.id,
                  },
                  create: {
                    licensePhotoId: file.id,
                  },
                },
              },
            },
          },
        },
      },
      context,
      '{id driver { id }}',
    )
    return { file, user }
  })
}
function verifyCurrentUser(context) {
  if (!context.currentUser) {
    throw lib_1.makeResponseError({
      message: 'You must be logged in to perform this action.',
      code: lib_1.ResponseCodes.BAD_REQUEST,
    })
  }
}
function updateUserProfile(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { userId, input } = args
    const user = yield getUserDetails(userId, context)
    if (input.photo) {
      yield updateProfilePhoto({ photo: input.photo, user }, context)
    }
    const locationData = {
      city:
        input.city || lib_1.optionalChaining(() => user.location.city) || null,
      state:
        input.state ||
        lib_1.optionalChaining(() => user.location.state) ||
        null,
      zip: input.zip || lib_1.optionalChaining(() => user.location.zip) || null,
      street: input.street
        ? `${input.apt ? `${input.apt} ` : ''}${input.street}`
        : lib_1.optionalChaining(() => user.location.street) || null,
      formattedAddress:
        input.address ||
        lib_1.optionalChaining(() => user.location.formattedAddress) ||
        null,
    }
    const update = {
      firstName: input.firstName || user.firstName,
      lastName: input.lastName || user.lastName,
      phone: input.phone || user.phone,
      location: {
        upsert: {
          create: locationData,
          update: locationData,
        },
      },
    }
    if (input.tncAccepted) {
      update.hyrecarTosAcceptedAt = lib_1.getDate(new Date())
      update.hyrecarTosAcceptedVersion = config_1.default.CURRENT_TOS_VERSION
    }
    if (input.about) {
      if (user.type === 'DRIVER') {
        update.driver = {
          update: {
            aboutMe: input.about,
          },
        }
      }
      if (user.type === 'OWNER') {
        update.owner = {
          update: {
            aboutMe: input.about,
          },
        }
      }
    }
    const updatedUser = yield context.users.mutation.updateUser(
      {
        where: {
          id: userId,
        },
        data: update,
      },
      context,
      `{
                id
                phone
                firstName
                lastName
                phone
                location{
                    id
                    zip
                    address
                    apt
                    street
                    city
                    state
                }
                hyrecarTosAcceptedAt
                type
                driver {
                    id
                    aboutMe
                }
                owner {
                    id
                    aboutMe
                }
            }`,
    )
    return updatedUser
  })
}
function updateProfilePhoto(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { photo, user } = args
    const file = yield upsertProfilePhotoFile(
      { file: photo, profilePhotoId: user.profilePhotoId },
      context,
    )
    const updatedUser = yield context.users.mutation.updateUser(
      {
        where: {
          id: user.id,
        },
        data: {
          profilePhotoId: file.id,
        },
      },
      context,
      '{id profilePhotoId}',
    )
    return { file, user: updatedUser }
  })
}
function upsertProfilePhotoFile(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    if (args.profilePhotoId) {
      return yield context.files.mutation.replaceFile(
        {
          data: {
            fileId: args.profilePhotoId,
            file: args.file,
            type: 'PROFILE_PHOTO',
          },
        },
        context,
        '{ id path url }',
      )
    }
    return yield context.files.mutation.uploadFile(
      {
        data: {
          file: args.file,
          type: 'PROFILE_PHOTO',
        },
      },
      context,
      '{ id path url }',
    )
  })
}
function getUserDetails(userId, context) {
  return __awaiter(this, void 0, void 0, function*() {
    return yield context.users.query.user(
      {
        where: {
          id: userId,
        },
      },
      context,
      `{
        id
        phone
        firstName
        lastName
        phone
        profilePhotoId
        location {
            id
            zip
            address
            apt
            street
            city
            state
        }
        hyrecarTosAcceptedAt
        type
        driver {
            id
            aboutMe
        }
        owner {
            id
            aboutMe
        }
    }`,
    )
  })
}
