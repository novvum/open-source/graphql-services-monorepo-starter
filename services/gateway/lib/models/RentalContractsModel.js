'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const getCarPriceBreakdown_1 = require('../schema/getCarPriceBreakdown')
const urlToPdfLink_1 = require('../utils/urlToPdfLink')
const queryString = require('querystring')
class RentalContractsModel {
  constructor(context) {
    this.context = context
  }
  generateContractUrl(args) {
    const { driver, car, application, owner } = args
    const query = queryString.stringify(
      Object.assign({}, car, driver, application, owner),
    )
    return `https://contracts.hyrecar.com/rental-contract?${query}`
  }
  getGeneralContractDetails(input) {
    return __awaiter(this, void 0, void 0, function*() {
      const driverId = input.driverId
      const rentalId = input.rentalId
      const rental = rentalId
        ? yield this.context.rentals.query.rental(
            {
              where: {
                id: rentalId,
              },
            },
            this.context,
            '{ id createdAt ownerApprovedAt }',
          )
        : {}
      const driver = yield this.getDriverContractDetails(
        driverId,
        rental.createdAt,
      )
      const car = yield this.getCarDetails(input.carId)
      const owner = yield this.getOwnerDetails(car, rental.ownerApprovedAt)
      return {
        driver,
        car,
        owner,
      }
    })
  }
  getCarDetails(carId) {
    return __awaiter(this, void 0, void 0, function*() {
      const carDetails = yield this.context.cars.query.car(
        {
          where: {
            id: carId,
          },
        },
        this.context,
        `{
            id
            make
            model
            year
            vin
            licensePlate
            dailyPriceInCents
            weeklyPriceInCents
            monthlyPriceInCents
            ownerId
            location {
                id
                state
            }
        }`,
      )
      const car = {
        id: carDetails.id,
        carState: carDetails.location ? carDetails.location.state : '',
        carMake: carDetails.make,
        carModel: carDetails.model,
        carYear: carDetails.year,
        carVin: carDetails.vin,
        carPlate: carDetails.licensePlate,
        carMaxDailyMiles: carDetails.maxDailyMiles,
        ownerId: carDetails.ownerId,
        dailyPriceInCents: carDetails.dailyPriceInCents,
        weeklyPriceInCents: carDetails.weeklyPriceInCents,
        monthlyPriceInCents: carDetails.monthlyPriceInCents,
      }
      return car
    })
  }
  getOwnerDetails(car, signingDate) {
    return __awaiter(this, void 0, void 0, function*() {
      const ownerDetails = yield this.context.users.query.owner(
        {
          where: {
            id: car.ownerId,
          },
        },
        this.context,
        `{
            id
            user {
                id
                firstName
                lastName
                phone
                location {
                    id
                    state
                }
            }
        }`,
      )
      const owner = {
        ownerPhone: ownerDetails.user.phone,
        ownerFullName: `${ownerDetails.user.firstName} ${
          ownerDetails.user.lastName
        }`,
        ownerState: ownerDetails.user.location.state,
        ownerSigningDate: signingDate || new Date().toISOString(),
      }
      return owner
    })
  }
  getDriverContractDetails(driverId, signingDate) {
    return __awaiter(this, void 0, void 0, function*() {
      const driverDetails = yield this.context.users.query.driver(
        {
          where: {
            id: driverId,
          },
        },
        this.context,
        `{ 
            id
            legalInfo {
                id
                licenseNumber
                licenseState
                fullName
            }
            user {
                id
                phone
                firstName
                lastName
                location {
                    id
                    formattedAddress
                }
            }
        }`,
      )
      const driver = {
        driverPhone:
          lib_1.optionalChaining(() => driverDetails.user.phone) || '',
        driverFullName:
          lib_1.optionalChaining(() => driverDetails.legalInfo.fullName) ||
          lib_1.optionalChaining(
            () =>
              `${driverDetails.user.firstName} ${driverDetails.user.lastName}`,
          ) ||
          '',
        driverAddress:
          lib_1.optionalChaining(
            () => driverDetails.user.location.formattedAddress,
          ) || '',
        driverLicenseNumber:
          lib_1.optionalChaining(() => driverDetails.legalInfo.licenseNumber) ||
          '',
        driverLicenseState:
          lib_1.optionalChaining(() => driverDetails.legalInfo.licenseState) ||
          '',
        driverSigningDate: signingDate || new Date().toISOString(),
      }
      return driver
    })
  }
  getProspectiveApplicationDetails(input, car) {
    return __awaiter(this, void 0, void 0, function*() {
      const priceBreakdown = yield this.context.payments.query.getPriceBreakdown(
        {
          where: {
            couponCode: input.couponCode,
            rentalDays: input.rentalDays,
            dailyPriceInCents: car.dailyPriceInCents,
            weeklyPriceInCents: car.weeklyPriceInCents,
            monthlyPriceInCents: car.monthlyPriceInCents,
            showDeposit: yield getCarPriceBreakdown_1.checkDepositRequired(
              { carId: car.id, driverUserId: this.context.currentUser.id },
              this.context,
            ),
          },
        },
        this.context,
        '{ id grandTotal refundableDeposit }',
      )
      const application = {
        applicationStartDate: new Date(input.startDate).toISOString(),
        applicationRentalDays: input.rentalDays,
        applicationTotalPriceInCents: priceBreakdown.grandTotal, //  + priceBreakdown.refundableDeposit,
      }
      return application
    })
  }
  getRentalPeriodContractDetails(rentalPeriod) {
    return __awaiter(this, void 0, void 0, function*() {
      let paymentInfo = {}
      if (rentalPeriod.paymentId) {
        paymentInfo = yield this.context.payments.query.rentalPayment(
          {
            where: {
              id: rentalPeriod.paymentId,
            },
          },
          this.context,
          '{ id grandTotalInCents }',
        )
      }
      const application = {
        applicationRentalDays: rentalPeriod.numDays,
        applicationStartDate: new Date(rentalPeriod.startDate).toISOString(),
        applicationTotalPriceInCents: paymentInfo.grandTotalInCents,
      }
      return application
    })
  }
  uploadRentalPeriodContract(args, context) {
    return __awaiter(this, void 0, void 0, function*() {
      const { driverId, rentalPeriodDetails } = args
      const pdfUrl = yield this.getPdfUrl(driverId, rentalPeriodDetails)
      const rentalContractFile = yield context.files.mutation.uploadFile(
        {
          data: {
            fileUrl: pdfUrl,
            type: 'RENTAL_CONTRACT',
          },
        },
        context,
        '{ id }',
      )
      const updatedPeriod = yield context.rentals.mutation.updateRentalPeriod(
        {
          where: {
            id: rentalPeriodDetails.id,
          },
          data: {
            rentalContractId: rentalContractFile.id,
          },
        },
        context,
        '{ id rentalContractId }',
      )
    })
  }
  getPdfUrl(driverId, rentalPeriodDetails) {
    return __awaiter(this, void 0, void 0, function*() {
      const contractDetails = yield this.getGeneralContractDetails({
        driverId: driverId,
        carId: rentalPeriodDetails.carId,
        rentalId: rentalPeriodDetails.rentalId,
      })
      const contractApplication = yield this.getRentalPeriodContractDetails({
        paymentId: rentalPeriodDetails.paymentId,
        numDays: rentalPeriodDetails.numDays,
        startDate: new Date(rentalPeriodDetails.startDate).toISOString(),
      })
      const url = yield this.generateContractUrl(
        Object.assign({}, contractDetails, {
          application: contractApplication,
        }),
      )
      const pdfUrl = urlToPdfLink_1.urlToPdfLink(url)
      return pdfUrl
    })
  }
  uploadSwitchedRentalPeriodContract(args, context) {
    return __awaiter(this, void 0, void 0, function*() {
      const { driverId, rentalPeriodDetails } = args
      const contractDetails = yield this.getGeneralContractDetails({
        driverId: driverId,
        carId: rentalPeriodDetails.carId,
        rentalId: rentalPeriodDetails.rentalId,
      })
      const contractApplication = {
        applicationRentalDays: rentalPeriodDetails.numDays,
        applicationStartDate: new Date(
          rentalPeriodDetails.startDate,
        ).toISOString(),
        applicationTotalPriceInCents: rentalPeriodDetails.totalPriceInCents,
      }
      const url = yield this.generateContractUrl(
        Object.assign({}, contractDetails, {
          application: contractApplication,
        }),
      )
      const pdfUrl = urlToPdfLink_1.urlToPdfLink(url)
      const rentalContractFile = yield context.files.mutation.uploadFile(
        {
          data: {
            fileUrl: pdfUrl,
            type: 'RENTAL_CONTRACT',
          },
        },
        context,
        '{ id }',
      )
      const updatedPeriod = yield context.rentals.mutation.updateRentalPeriod(
        {
          where: {
            id: rentalPeriodDetails.id,
          },
          data: {
            rentalContractId: rentalContractFile.id,
          },
        },
        context,
        '{ id rentalContractId }',
      )
    })
  }
}
exports.default = RentalContractsModel
