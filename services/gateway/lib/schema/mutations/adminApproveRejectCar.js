'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
exports.linkTypeDefs = `
    type AdminApproveRejectCarPayload {
        code: String
        success: Boolean
        message: String
        car: Car
    }
    input AdminApproveRejectCarInput {
        carId: ID!
    }
    extend type Mutation {
        adminRejectCar(where: AdminApproveRejectCarInput!): AdminApproveRejectCarPayload
        adminApproveCar(where: AdminApproveRejectCarInput!): AdminApproveRejectCarPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    adminRejectCar(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const updatedCar = yield context.cars.mutation.updateCar(
            {
              where: {
                id: args.where.carId,
              },
              data: {
                status: 'REMOVED',
                verification: 'FAILED_VERIFICATION',
              },
            },
            context,
            '{ id status verification }',
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            car: updatedCar,
          }
        } catch (e) {
          return {
            code: lib_1.ResponseCodes.REQUEST_FAILED,
            success: false,
            message: e.message,
          }
        }
      })
    },
    adminApproveCar(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const updatedCar = yield context.cars.mutation.updateCar(
            {
              where: {
                id: args.where.carId,
              },
              data: {
                status: 'AVAILABLE',
                verification: 'VERIFIED',
              },
            },
            context,
            '{ id status verification }',
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            car: updatedCar,
          }
        } catch (e) {
          return {
            code: lib_1.ResponseCodes.REQUEST_FAILED,
            success: false,
            message: e.message,
          }
        }
      })
    },
  },
})
