'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type Rental {
    driverRating: Rating
    carRating: Rating
  }
  extend type Rating {
    rental: Rental
}
`
exports.resolvers = mergeInfo => ({
  Rating: {
    rental: {
      fragment: `fragment RentalFragment on Rating { rentalId }`,
      resolve(parent, _, context, info) {
        const id = parent.rentalId
        return mergeInfo.delegate(
          'query',
          'rental',
          {
            where: {
              id: id,
            },
          },
          context,
          info,
        )
      },
    },
  },
  Rental: {
    driverRating: {
      fragment: `fragment RatingFragment on Rental { id }`,
      resolve(parent, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const rentalId = parent.id
          const ratings = yield context.ratings.query.ratings(
            {
              where: {
                rentalId: rentalId,
                type: 'DRIVER_RATING',
              },
              first: 1,
            },
            context,
            '{ id }',
          )
          if (ratings.length === 0) {
            return null
          }
          return yield context.ratings.query.rating(
            {
              where: {
                id: ratings[0].id,
              },
            },
            context,
            info,
          )
        })
      },
    },
    carRating: {
      fragment: `fragment RatingFragment on Rental { id }`,
      resolve(parent, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const rentalId = parent.id
          const ratings = yield context.ratings.query.ratings(
            {
              where: {
                rentalId: rentalId,
                type: 'CAR_RATING',
              },
              first: 1,
            },
            context,
            '{ id }',
          )
          if (ratings.length === 0) {
            return null
          }
          return yield context.ratings.query.rating(
            {
              where: {
                id: ratings[0].id,
              },
            },
            context,
            info,
          )
        })
      },
    },
  },
})
