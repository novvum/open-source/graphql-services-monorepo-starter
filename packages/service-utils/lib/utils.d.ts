export declare function getDate(date: any, func?: any): string
export declare class AuthError extends Error {
  constructor()
}
export declare const loadEnv: (env: any) => string
export declare function optionalChaining(func: any): any
