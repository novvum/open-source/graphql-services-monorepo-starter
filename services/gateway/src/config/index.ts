import { Config } from 'service-utils/lib'
export interface GatewayConfig extends Config {
  [key: string]: any
}
const config: GatewayConfig = {
  test: {},
  dev: {},
  staging: {},
  production: {},
  disableEngine: true,
  PORT: parseInt(process.env.PORT || '4000', 10),
}

export default {
  ...config,
  ...config[process.env.GRAPHQL_STAGE],
}
