import { Options } from 'graphql-yoga'

export default function getYogaConfig(
  yogaConfig: Pick<
    Options,
    | 'port'
    | 'cors'
    | 'uploads'
    | 'endpoint'
    | 'subscriptions'
    | 'playground'
    | 'deduplicator'
    | 'getEndpoint'
    | 'tracing'
    | 'cacheControl'
    | 'formatError'
    | 'logFunction'
    | 'rootValue'
    | 'validationRules'
    | 'fieldResolver'
    | 'formatParams'
    | 'formatResponse'
    | 'debug'
  >,
  path = '/',
): Pick<
  Options,
  | 'port'
  | 'cors'
  | 'uploads'
  | 'endpoint'
  | 'subscriptions'
  | 'playground'
  | 'deduplicator'
  | 'getEndpoint'
  | 'tracing'
  | 'cacheControl'
  | 'formatError'
  | 'logFunction'
  | 'rootValue'
  | 'validationRules'
  | 'fieldResolver'
  | 'formatParams'
  | 'formatResponse'
  | 'debug'
> {
  return {
    tracing: true,
    cacheControl: true,
    ...(yogaConfig || {}),
    playground: false,
    // endpoint: path
  }
}
