'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
    extend type RentalPayment {
        rentalPeriod: RentalPeriod
    }
    extend type RentalPeriod {
        payment: RentalPayment
    }
    extend type Deposit {
        rental: Rental
    }
    extend type Rental {
        deposit: Deposit
    }
`
exports.resolvers = mergeInfo => ({
  Deposit: {
    rental: {
      fragment: `fragment RentalFragment on Deposit { rentalId }`,
      resolve(deposit, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const rentalId = deposit.rentalId
          if (!rentalId) {
            return null
          }
          return context.rentals.query.rental(
            {
              where: {
                id: rentalId,
              },
            },
            context,
            info,
          )
        })
      },
    },
  },
  Rental: {
    deposit: {
      fragment: `fragment DepositFragment on Rental { id depositId }`,
      resolve(rental, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const depositId = rental.depositId
          if (!depositId) {
            return null
          }
          return context.payments.query.deposit(
            {
              where: {
                rentalId: rental.id,
              },
            },
            context,
            info,
          )
        })
      },
    },
  },
  RentalPayment: {
    rentalPeriod: {
      fragment: `fragment RentalPaymentFragment on RentalPayment { rentalPeriodId }`,
      resolve(rentalPayment, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const rentalPeriodId = rentalPayment.rentalPeriodId
          if (!rentalPeriodId) {
            return null
          }
          return context.rentals.query.rentalPeriod(
            {
              where: {
                id: rentalPeriodId,
              },
            },
            context,
            info,
          )
        })
      },
    },
  },
  RentalPeriod: {
    payment: {
      fragment: `fragment RentalPaymentFragment on RentalPeriod { id }`,
      resolve(rentalPeriod, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          return context.payments.query.rentalPayment(
            {
              where: {
                rentalPeriodId: rentalPeriod.id,
              },
            },
            context,
            info,
          )
        })
      },
    },
  },
})
