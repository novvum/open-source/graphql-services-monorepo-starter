'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type Insurance {
      rental: Rental
      car: Car
  }
  extend type Rental {
    insurance: Insurance
    insurances(filter: InsuranceWhereInput, orderBy: InsuranceOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Insurance]!
    insurancesConnection(filter: InsuranceWhereInput, orderBy: InsuranceOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): InsuranceConnection!
	}
`
exports.resolvers = mergeInfo => ({
  Insurance: {
    car: {
      fragment: `fragment CarInsuranceFragment on Insurance { id carId rentalId }`,
      resolve(parent, _, context, info) {
        const rentalId = parent.carId
        return mergeInfo.delegate(
          'query',
          'car',
          {
            where: {
              id: rentalId,
            },
          },
          context,
          info,
        )
      },
    },
    rental: {
      fragment: `fragment RentalFragment on Insurance { id rentalId }`,
      resolve(parent, _, context, info) {
        const rentalId = parent.rentalId
        return mergeInfo.delegate(
          'query',
          'rental',
          {
            where: {
              id: rentalId,
            },
          },
          context,
          info,
        )
      },
    },
  },
  Rental: {
    insurance: {
      fragment: `fragment InsuranceFragment on Rental { id carId }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.id
          const insurances = yield context.insurance.query.insurances(
            {
              first: 1,
              where: {
                rentalId: id,
                carId: parent.carId,
              },
              orderBy: 'createdAt_DESC',
            },
            context,
            '{ id }',
          )
          if (insurances.length === 0) {
            return null
          }
          return context.insurance.query.insurance(
            {
              where: {
                id: insurances[0].id,
              },
            },
            context,
            info,
          )
        })
      },
    },
    insurances: {
      fragment: `fragment InsuranceFragment on Rental { id }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.id
          return context.insurance.query.insurances(
            Object.assign({}, args, {
              where: {
                AND: [
                  args.filter || {},
                  {
                    rentalId: id,
                  },
                ],
              },
            }),
            context,
            info,
          )
        })
      },
    },
    insurancesConnection: {
      fragment: `fragment InsuranceFragment on Rental { id }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.id
          return context.insurance.query.insurancesConnection(
            Object.assign({}, args, {
              where: {
                AND: [
                  args.filter || {},
                  {
                    rentalId: id,
                  },
                ],
              },
            }),
            context,
            info,
          )
        })
      },
    },
  },
})
