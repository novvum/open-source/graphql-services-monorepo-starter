'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type Viewer {
    rentals(filter: RentalWhereInput, orderBy: RentalOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): RentalConnection!
  }
  extend type Rental {
      driver: Driver
  }
  extend type Driver {
    rentals(filter: RentalWhereInput, orderBy: RentalOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): RentalConnection!
  }
  extend type Owner {
    rentalsConnection(filter: RentalWhereInput, orderBy: RentalOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): RentalConnection!
    rentals(filter: RentalWhereInput, orderBy: RentalOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Rental]!
  }
`
exports.resolvers = mergeInfo => ({
  Rental: {
    driver: {
      fragment: `fragment DriverFragment on Rental { driverId }`,
      resolve(parent, _, context, info) {
        const driverId = parent.driverId
        if (!driverId) {
          return null
        }
        return mergeInfo.delegate(
          'query',
          'driver',
          {
            where: {
              id: driverId,
            },
          },
          context,
          info,
        )
      },
    },
  },
  Driver: {
    rentals: {
      fragment: `fragment RentalFragment on Driver { id }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.id
          // TODO: get list of owner's car id's for OR condition below
          return mergeInfo.delegate(
            'query',
            'rentalsConnection',
            Object.assign({}, args, {
              where: {
                AND: [{ driverId: id }, args.filter || {}],
              },
            }),
            context,
            info,
          )
        })
      },
    },
  },
  Owner: {
    rentalsConnection: {
      fragment: `fragment OwnerRentalFragment on Owner { id }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.id
          const carIds = yield getOwnerCarIds(id, context)
          // TODO: get list of owner's car id's for OR condition below
          return context.rentals.query.rentalsConnection(
            Object.assign({}, args, {
              where: {
                AND: [{ carId_in: carIds }, args.filter || {}],
              },
            }),
            context,
            info,
          )
        })
      },
    },
    rentals: {
      fragment: `fragment OwnerRentalFragment on Owner { id }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.id
          const carIds = yield getOwnerCarIds(id, context)
          // TODO: get list of owner's car id's for OR condition below
          return context.rentals.query.rentals(
            Object.assign({}, args, {
              where: {
                AND: [{ carId_in: carIds }, args.filter || {}],
              },
            }),
            context,
            info,
          )
        })
      },
    },
  },
  Viewer: {
    rentals: {
      fragment: `fragment ViewerFragment on Viewer { me { driver { id } } }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id =
            parent.me &&
            parent.me.driver &&
            parent.me.driver &&
            parent.me.driver.id
          // TODO: get list of owner's car id's for OR condition below
          return mergeInfo.delegate(
            'query',
            'rentalsConnection',
            Object.assign({}, args, {
              where: {
                AND: [
                  {
                    OR: [{ driverId: id }],
                  },
                  args.filter || {},
                ],
              },
            }),
            context,
            info,
          )
        })
      },
    },
  },
})
function getOwnerCarIds(ownerId, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const ownerCars = yield context.cars.query.cars(
      {
        where: {
          ownerId,
        },
      },
      context,
      '{ id }',
    )
    const carIds = ownerCars.map(c => c.id)
    return carIds
  })
}
