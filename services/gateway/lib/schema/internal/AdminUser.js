'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type Admin {
      user: User
  }
  extend type User {
		admin: Admin
	}
	extend type Viewer {
		admin: Admin
	}
`
exports.resolvers = mergeInfo => ({
  Admin: {
    user: {
      fragment: `fragment UserFragment on Admin { userId }`,
      resolve(parent, _, context, info) {
        const userId = parent.userId
        return mergeInfo.delegate(
          'query',
          'user',
          {
            where: {
              id: userId,
            },
          },
          context,
          info,
        )
      },
    },
  },
  User: {
    admin: {
      fragment: `fragment UserFragment on Admin { userId }`,
      resolve(parent, _, context, info) {
        const userId = parent.id
        return mergeInfo.delegate(
          'query',
          'admin',
          {
            where: {
              userId: userId,
            },
          },
          context,
          info,
        )
      },
    },
  },
  Viewer: {
    admin: {
      fragment: `fragment ViewerFragment on Admin { userId }`,
      resolve(parent, _, context, info) {
        const userId = parent.me.id
        return mergeInfo.delegate(
          'query',
          'admin',
          {
            where: {
              userId: userId,
            },
          },
          context,
          info,
        )
      },
    },
  },
})
