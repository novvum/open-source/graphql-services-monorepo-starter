'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type Driver {
    claims(filter: ClaimWhereInput, orderBy: ClaimOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): ClaimConnection
    driverClaimsMeta: DriverClaimsMeta
  }
`
exports.resolvers = mergeInfo => ({
  Driver: {
    claims: {
      fragment: `fragment ClaimFragment on Driver { id }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.id
          if (!id) {
            return null
          }
          const rentals = yield context.rentals.query.rentals(
            {
              where: {
                driverId: id,
              },
            },
            context,
            '{ id }',
          )
          return context.claim.query.claimConnection(
            {
              where: {
                AND: [
                  {
                    rentalId_in: rentals.map(rental => rental.id),
                  },
                  args.filter,
                ],
              },
            },
            context,
            info,
          )
        })
      },
    },
    driverClaimsMeta: {
      fragment: `fragment ClaimFragment on Driver { id }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.id
          if (!id) {
            return null
          }
          return context.admin.query.driverClaimsMeta(
            {
              driverId: id,
            },
            context,
            info,
          )
        })
      },
    },
  },
})
