'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
var moment = require('moment')
exports.linkTypeDefs = `
  type RentalOverview {
    id: ID!
    rentalDays: Int
    startDate: DateTime
	endDate: DateTime
	daysRemaining: Int
	hoursRemaining: Int
    ownerEarningsInCents: Int
    totalCostInCents: Int
	totalPaidInCents: Int
	lateFeeCaptured: Boolean
  }
  extend type Rental {
    overview: RentalOverview
  }
`
exports.resolvers = mergeInfo => ({
  Rental: {
    overview: {
      fragment: `fragment RentalOverivewFragment on Rental {
                id
            }`,
      resolve(parent, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const rentalId = parent.id
          const overview = yield getRentalOverview(rentalId, context)
          return overview
        })
      },
    },
  },
})
function getRentalOverview(rentalId, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const rental = yield context.rentals.query.rental(
      {
        where: {
          id: rentalId,
        },
      },
      context,
      `{
					id
					rentalPeriods(orderBy: endDate_DESC) {
						id
						numDays
						startDate
						endDate
						paymentId
					}
				}`,
    )
    const periods = yield Promise.all(
      rental.rentalPeriods.map(period =>
        __awaiter(this, void 0, void 0, function*() {
          return Object.assign({}, period, {
            payment: period.paymentId
              ? yield context.payments.query.rentalPayment(
                  {
                    where: {
                      id: period.paymentId,
                    },
                  },
                  context,
                  '{ id ownerEarningsInCents grandTotalInCents chargeId rentalLateFeePayment { id chargeId }}',
                )
              : {},
          })
        }),
      ),
    )
    const overview = periods.reduce(
      (accumulator, period) => {
        accumulator.rentalDays = accumulator.rentalDays + period.numDays
        accumulator.startDate =
          accumulator.startDate && accumulator.startDate < period.startDate
            ? accumulator.startDate
            : period.startDate
        accumulator.endDate =
          accumulator.endDate && accumulator.endDate > period.endDate
            ? accumulator.endDate
            : period.endDate
        accumulator.ownerEarningsInCents =
          accumulator.ownerEarningsInCents +
          (period.payment && period.payment.ownerEarningsInCents
            ? period.payment.ownerEarningsInCents
            : 0)
        accumulator.totalCostInCents =
          accumulator.totalCostInCents +
          (period.payment && period.payment.grandTotalInCents
            ? period.payment.grandTotalInCents
            : 0)
        accumulator.totalPaidInCents =
          accumulator.totalPaidInCents +
          (period.payment && period.payment.chargeId
            ? period.payment.grandTotalInCents
            : 0)
        accumulator.daysRemaining = moment(accumulator.endDate).diff(
          new Date(),
          'days',
        )
        accumulator.hoursRemaining = moment(accumulator.endDate).diff(
          new Date(),
          'hours',
        )
        accumulator.totalDays = accumulator.totalDays + period.numDays
        return accumulator
      },
      {
        id: `ov${rentalId}`,
        rentalDays: 0,
        startDate: null,
        endDate: null,
        ownerEarningsInCents: 0,
        totalCostInCents: 0,
        totalPaidInCents: 0,
        daysRemaining: 0,
        hoursRemaining: 0,
        totalDays: 0,
        lateFeeCaptured: false,
      },
    )
    overview.lateFeeCaptured = !!lib_1.optionalChaining(
      () => periods[0].payment.rentalLateFeePayment.chargeId,
    )
    return overview
  })
}
exports.getRentalOverview = getRentalOverview
