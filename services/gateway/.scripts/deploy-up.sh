#!/bin/bash

set -ex

# ENV Variables, Note: ACCESS_TOKEN and NOW_TOKEN in gitlab-ci
ORIGIN='*'

export PATH="./node_modules/.bin:$PATH"

node ./.scripts/preup.js

up deploy $UP_STAGE

# up stack plan
# up stack apply
# up stack
up url $1