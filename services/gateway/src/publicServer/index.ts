import { GraphQLServer, OptionsWithoutHttps, Options } from 'graphql-yoga'
import { getContext } from '../getContext'
import mergedSchemas from '../schema'
import { transformSchema } from 'graphql-transform-schema'
import config from '../config'
import publicTransforms from './schemaTransforms'
import { mocks } from '../mocks'
import { startApolloEngine } from '../utils/apolloEngine'
import getYogaConfig from '../utils/getYogaConfig'

export default (yogaConfig: OptionsWithoutHttps, path?) => {
  // NOTE: if we need apollo engine in the future: https://github.com/playerx/graphql-yoga-sample/blob/master/src/index.ts
  const schema =
    config.GRAPHQL_ENABLE_MOCKS === 'true'
      ? mergedSchemas.getMockedSchema(mocks as any)
      : mergedSchemas.getSchema()
  // hide every query/mutation except the `Starship` and `allStarships` query

  //   const transformedSchema = transformSchema(schema as any, publicTransforms)
  const server = new GraphQLServer({
    schema, //: transformedSchema as any,
    context: getContext,
  })
  server.createHttpServer(getYogaConfig(yogaConfig, path))
  if (!path) {
    startApolloEngine({
      port: yogaConfig.port,
      serverType: 'api',
      expressApp: server.express,
    })
  }
  return server
}
