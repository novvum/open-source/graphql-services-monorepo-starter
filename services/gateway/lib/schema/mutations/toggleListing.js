'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const approveRejectBooking_1 = require('./approveRejectBooking')
exports.linkTypeDefs = `
    input ToggleListingInput {
        carId: ID!
    }
    type ToggleListingPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        car: Car
    }
    extend type Mutation {
        toggleListing(data: ToggleListingInput!): ToggleListingPayload
        adminToggleListing(data: ToggleListingInput!): ToggleListingPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    toggleListing(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const { car } = yield getRequiredData(context, args)
          yield validateRequest(car, context)
          const updatedCar = yield toggleCarMarketStatus(car, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            car: updatedCar,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      })
    },
    adminToggleListing(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const { car } = yield getRequiredData(context, args)
          const updatedCar = yield toggleCarMarketStatus(car, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            car: updatedCar,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      })
    },
  },
})
function validateRequest(car, context) {
  return __awaiter(this, void 0, void 0, function*() {
    if (
      !(yield approveRejectBooking_1.isCurrentUserCarOwner(car.id, context))
    ) {
      throw lib_1.makeResponseError({
        code: lib_1.NOT_AUTHORIZED_RESPONSE.code,
        message: lib_1.NOT_AUTHORIZED_RESPONSE.message,
      })
    }
  })
}
function getRequiredData(context, args) {
  return __awaiter(this, void 0, void 0, function*() {
    return {
      car: yield context.cars.query.car(
        {
          where: {
            id: args.data.carId,
          },
        },
        context,
        '{ id status }',
      ),
    }
  })
}
function isCarTogglable(carStatus) {
  return ['AVAILABLE', 'REMOVED'].includes(carStatus)
}
function toggleCarMarketStatus(car, context) {
  return __awaiter(this, void 0, void 0, function*() {
    if (!isCarTogglable(car.status)) {
      throw lib_1.makeResponseError({
        code: lib_1.ResponseCodes.BAD_REQUEST,
        message: `You cannot do this for a car with the status ${car.status}.`,
      })
    }
    if (car.status === 'AVAILABLE') {
      return yield context.cars.mutation.removeCarFromMarket(
        {
          data: {
            carId: car.id,
          },
        },
        context,
        '{ id status }',
      )
    }
    if (car.status === 'REMOVED') {
      return yield context.cars.mutation.addCarToMarket(
        {
          data: {
            carId: car.id,
          },
        },
        context,
        '{ id status }',
      )
    }
    return null
  })
}
