'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type Car {
    claims(filter: ClaimWhereInput, orderBy: ClaimOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Claim!]
  }
  extend type Claim {
    car: Car
}
`
exports.resolvers = mergeInfo => ({
  Claim: {
    car: {
      fragment: `fragment CarFragment on Claim { carId }`,
      resolve(parent, _, context, info) {
        const id = parent.carId
        return mergeInfo.delegate(
          'query',
          'car',
          {
            where: {
              id: id,
            },
          },
          context,
          info,
        )
      },
    },
  },
  Car: {
    claims: {
      fragment: `fragment ClaimFragment on Car { id }`,
      resolve(parent, args, context, info) {
        const id = parent.id
        return mergeInfo.delegate(
          'query',
          'claims',
          Object.assign({}, args, {
            where: {
              carId: id,
            },
          }),
          context,
          info,
        )
      },
    },
  },
})
