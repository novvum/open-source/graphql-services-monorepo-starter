'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const urlToPdfLink_1 = require('../utils/urlToPdfLink')
const oChain_1 = require('../utils/oChain')
const queryString = require('querystring')
class FNOLModel {
  constructor(ctx) {
    this.ctx = ctx
  }
  generateFNOLUrl(args) {
    const { driver, car, claim, rental, owner } = args
    const query = queryString.stringify(
      Object.assign({}, car, driver, claim, rental, owner),
    )
    return `https://contracts.hyrecar.com/fnol?${query}`
  }
  getGeneralContractDetails(input) {
    return __awaiter(this, void 0, void 0, function*() {
      const { claimId } = input
      try {
        const claimDetails = yield this.ctx.admin.query.claim(
          {
            where: {
              id: claimId,
            },
          },
          this.ctx,
          `{
    id
    driverId
    rentalId
    ownerId
    carId
    insuranceId
    accident{
      damageLocation
      location{
        street
        city
        zip
        state
      }
      factOfLoss{  
        policeReport{
          policeDepartment
          reportNumber
        }
        description
        dateOfIncident
      }
      tncClaim
      tncUsageDetails{
        tncStage
        passengerIdentifications
        passengers
      }
    }

  }`,
        )
        const claimRental = yield this.ctx.rentals.query.rental(
          {
            where: {
              id: claimDetails.rentalId,
            },
          },
          this.ctx,
          `{
          droppedOffAt
          rentalPeriods(where:{type:INITIAL}){
            startDate
          }
        }`,
        )
        const claimDriver = yield this.ctx.users.query.driver(
          {
            where: {
              id: claimDetails.driverId,
            },
          },
          this.ctx,
          `{
          id
          legalInfo{
            id
            licenseNumber
            dob
          }
          user {
            email
            phone
            firstName
            lastName
            location{
              city
              street
              zip
              state
            }
          }
        }`,
        )
        const claimOwner = yield this.ctx.users.query.owner(
          {
            where: {
              id: claimDetails.ownerId,
            },
          },
          this.ctx,
          `{
        id
        user{
          id
          email
          phone
          firstName
          lastName
          location{
            formattedAddress
          }
        }
      }`,
        )
        const claimCar = yield this.ctx.cars.db.query.car({
          where: {
            id: claimDetails.carId,
          },
        })
        const claim = {
          claimTncClaim:
            lib_1.optionalChaining(() => claimDetails.accident.tncClaim) ||
            false,
          claimTncStage:
            lib_1.optionalChaining(
              () => claimDetails.accident.tncUsageDetails.tncStage,
            ) || 'N/A',
          claimPassengers:
            lib_1.optionalChaining(
              () => claimDetails.accident.tncUsageDetails.passengers,
            ) || 'N/A',
          claimPassengerIdentifications:
            lib_1.optionalChaining(
              () =>
                claimDetails.accident.tncUsageDetails.passengerIdentifications,
            ) || 'N/A',
          claimAccidentDetails:
            lib_1.optionalChaining(
              () => claimDetails.accident.factOfLoss.description,
            ) || 'N/A',
          claimPoliceReportNumber:
            lib_1.optionalChaining(
              () => claimDetails.accident.factOfLoss.policeReport.reportNumber,
            ) || 'N/A',
          claimPoliceReport:
            lib_1.optionalChaining(
              () =>
                claimDetails.accident.factOfLoss.policeReport.policeDepartment,
            ) || 'N/A',
          claimAccidentLocation: oChain_1.mapLocation(
            claimDetails.accident.location,
          ),
          claimDateOfLoss:
            lib_1.optionalChaining(
              () => claimDetails.accident.factOfLoss.dateOfIncident,
            ) || 'N/A',
        }
        const rental = {
          rentalPickupDate:
            lib_1.optionalChaining(
              () => claimRental.rentalPeriods[0].startDate,
            ) || 'N/A',
          rentalDropoffDate:
            lib_1.optionalChaining(() => claimRental.droppedOffAt) || 'N/A',
        }
        const ownerFirstName =
          lib_1.optionalChaining(() => claimOwner.user.firstName) || ''
        const ownerLastName =
          lib_1.optionalChaining(() => claimOwner.user.lastName) || ''
        const owner = {
          ownerPhone:
            lib_1.optionalChaining(() => claimOwner.user.phone) || 'N/A',
          ownerFullName: `${ownerFirstName} ${ownerLastName}` || 'N/A',
          ownerAddress:
            lib_1.optionalChaining(
              () => claimOwner.user.location.formattedAddress,
            ) || 'N/A',
          ownerEmail:
            lib_1.optionalChaining(() => claimOwner.user.email) || 'N/A',
        }
        const driverStreet =
          lib_1.optionalChaining(() => claimDriver.user.location.street) || ''
        const driverCity =
          lib_1.optionalChaining(() => claimDriver.user.location.city) || ''
        const driverState =
          lib_1.optionalChaining(() => claimDriver.user.location.state) || ''
        const driverZip =
          lib_1.optionalChaining(() => claimDriver.user.location.zip) || ''
        const driver = {
          driverPhone:
            lib_1.optionalChaining(() => claimDriver.user.phone) || 'N/A',
          driverFirstName:
            lib_1.optionalChaining(() => claimDriver.user.firstName) || 'N/A',
          driverLastName:
            lib_1.optionalChaining(() => claimDriver.user.lastName) || 'N/A',
          driverLicenseNumber:
            lib_1.optionalChaining(() => claimDriver.legalInfo.licenseNumber) ||
            'N/A',
          driverEmail:
            lib_1.optionalChaining(() => claimDriver.user.email) || 'N/A',
          driverAddress:
            `${driverStreet} ${driverCity} ${driverState} ${driverZip}` ||
            'N/A',
          driverDob:
            lib_1.optionalChaining(() => claimDriver.legalInfo.dob) || 'N/A',
        }
        const car = {
          carMake: lib_1.optionalChaining(() => claimCar.make) || 'N/A',
          carModel: lib_1.optionalChaining(() => claimCar.model) || 'N/A',
          carYear: lib_1.optionalChaining(() => claimCar.year) || 'N/A',
          carVin: lib_1.optionalChaining(() => claimCar.vin) || 'N/A',
          carPlate:
            lib_1.optionalChaining(() => claimCar.licensePlate) || 'N/A',
          carDamage:
            lib_1.optionalChaining(
              () => claimDetails.accident.damageLocation,
            ) || 'N/A',
        }
        return {
          driver,
          car,
          owner,
          rental,
          claim,
        }
      } catch (err) {
        console.log(err)
        lib_1.makeResponseError(err)
      }
    })
  }
  getFNOLPdf(input) {
    return __awaiter(this, void 0, void 0, function*() {
      try {
        const details = yield this.getGeneralContractDetails(input)
        const fnolUrl = yield this.generateFNOLUrl(Object.assign({}, details))
        const pdfUrl = urlToPdfLink_1.urlToPdfLink(fnolUrl)
        return pdfUrl
      } catch (err) {
        console.error(err)
        lib_1.makeResponseError(err)
      }
    })
  }
}
exports.default = FNOLModel
