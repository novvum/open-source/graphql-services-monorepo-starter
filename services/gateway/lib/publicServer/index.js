'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const graphql_yoga_1 = require('graphql-yoga')
const getContext_1 = require('../getContext')
const schema_1 = require('../schema')
const graphql_transform_schema_1 = require('graphql-transform-schema')
const config_1 = require('../config')
const schemaTransforms_1 = require('./schemaTransforms')
const mocks_1 = require('../mocks')
const apolloEngine_1 = require('../utils/apolloEngine')
const getYogaConfig_1 = require('../utils/getYogaConfig')
// import { graphiqlExpress } from 'apollo-server-express';
// const timber = require('timber');
// const transport = new timber.transports.HTTPS(process.env.PUBLIC_TIMBER_KEY);
// timber.install(transport);
// server.express.use(timber.middlewares.express());
exports.default = (yogaConfig, path) => {
  // NOTE: if we need apollo engine in the future: https://github.com/playerx/graphql-yoga-sample/blob/master/src/index.ts
  const schema =
    config_1.default.GRAPHQL_ENABLE_MOCKS === 'true'
      ? schema_1.default.getMockedSchema(mocks_1.mocks)
      : schema_1.default.getSchema()
  // hide every query/mutation except the `Starship` and `allStarships` query
  const transformedSchema = graphql_transform_schema_1.transformSchema(
    schema,
    schemaTransforms_1.default,
  )
  const server = new graphql_yoga_1.GraphQLServer({
    schema: transformedSchema,
    context: getContext_1.getContext,
  })
  server.express.use(getContext_1.context.users.auth.middleware)
  server.createHttpServer(getYogaConfig_1.default(yogaConfig, path))
  if (!path) {
    apolloEngine_1.startApolloEngine({
      port: yogaConfig.port,
      serverType: 'api',
      server,
    })
  }
  return server
}
