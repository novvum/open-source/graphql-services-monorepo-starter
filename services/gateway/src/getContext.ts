import * as express from 'express'
import config from './config'
import { ServicesContext, Request, Response } from 'service-utils/lib'
import {
  PrismaSampleBinding,
  PrismaSampleContext,
} from '@services/prisma-sample'

export interface MergedContext extends ServicesContext, PrismaSampleContext {}

const DEBUG = false // process.env.GRAPHQL_STAGE === "dev";

export interface JwtRequest extends Request {
  user?: any
}
const context = getInitializedContext()
const getContext = async (args: {
  request: JwtRequest
  response: Response
}) => {
  const sessionContext = {
    ...context,
  }
  sessionContext.request = args.request

  return sessionContext
}

function getInitializedContext() {
  const context = {} as MergedContext
  const prismaSampleBinding = new PrismaSampleBinding({
    debug: DEBUG,
    prismaEndpoint:
      'https://us1.prisma.sh/public-bittergargoyle-918/prisma-sample/dev',
    prismaSecret: 'mysecret123',
  })
  context.prismaSample = prismaSampleBinding
  return context
}

export { context, getContext }
