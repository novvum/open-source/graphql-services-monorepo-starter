/**
200 - OK Everything worked as expected.

400 - Bad Request The request was unacceptable, often due to missing a required parameter.

401 - Unauthorized No valid API key provided.

402 - Request Failed The parameters were valid but the request failed.

404 - Not Found The requested resource doesn't exist.

409 - Conflict The request conflicts with another request (perhaps due to using the same idempotent key).

429 - Too Many Requests Too many requests hit the API too quickly. We recommend an exponential backoff of your requests.

500, 502, 503, 504 - Server Errors Something went wrong on HyreCar's end. (These should be rare.)

 */
export declare const ResponseCodes: {
  OK: number
  BAD_REQUEST: number
  UNAUTHORIZED: number
  REQUEST_FAILED: number
  FORBIDDEN: number
  NOT_FOUND: number
  CONFLICT: number
  TOO_MANY_REQUESTS: number
  serverError: {
    GENERIC: number
    BAD_GATEWAY: number
    SERVICE_NOT_AVAILABLE: number
    TIMEOUT: number
  }
}
export declare const NOT_AUTHORIZED_RESPONSE: {
  code: number
  success: boolean
  message: string
  rental: any
}
export declare const CARSWITCH_UNAVAILABLE: {
  code: number
  success: boolean
  message: string
  rental: any
}
export declare const CARSWITCH_UNVERIFIED: {
  code: number
  success: boolean
  message: string
  rental: any
}
export declare const CARSWITCH_LATE_RENTAL: {
  code: number
  success: boolean
  message: string
  rental: any
}
