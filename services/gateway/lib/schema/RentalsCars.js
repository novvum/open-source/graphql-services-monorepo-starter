'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type Car {
    rentals(filter: RentalWhereInput, orderBy: RentalOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): RentalConnection!
  }
  extend type Rental {
    car: Car!
  }
`
exports.resolvers = mergeInfo => ({
  Car: {
    rentals: {
      fragment: `fragment CarFragment on Car { id }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.id
          if (!id) {
            return null
          }
          // TODO: get list of owner's car id's for OR condition below
          return mergeInfo.delegate(
            'query',
            'rentalsConnection',
            Object.assign({}, args, {
              where: {
                AND: [{ carId: id }, args.filter || {}],
              },
            }),
            context,
            info,
          )
        })
      },
    },
  },
  Rental: {
    car: {
      fragment: `fragment CarFragment on Rental { id carId }`,
      resolve(parent, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.carId
          if (!id) {
            return {
              id,
            }
          }
          // TODO: get list of owner's car id's for OR condition below
          return mergeInfo.delegate(
            'query',
            'car',
            {
              where: {
                id: id,
              },
            },
            context,
            info,
          )
        })
      },
    },
  },
})
