'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
exports.linkTypeDefs = `
    input UpdatePaymentInfoInput {
        token: String!
    }
    type UpdatePaymentInfoPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        paymentAccount: PaymentAccount
    }
    extend type Mutation {
        """Checks if user already has payment info, and if so, assigns the source. Otherwise, creates new paymentInfo"""
        updatePaymentInfo(data: UpdatePaymentInfoInput!): UpdatePaymentInfoPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    updatePaymentInfo(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const userData = yield context.users.query.user(
            {
              where: {
                id: context.currentUser.id,
              },
            },
            context,
            `{
          id
          firstName
          lastName
          email
          phone
          location {
            id
            street
            city
            state
            zip
          }
        }`,
          )
          const paymentAccount = yield context.payments.mutation.upsertUserPaymentInfo(
            {
              data: {
                userId: context.currentUser.id,
                token: args.data.token,
                fullName: `${userData.firstName} ${userData.lastName}`,
                email: userData.email,
                phone: userData.phone,
                street: lib_1.optionalChaining(() => userData.location.street),
                city: lib_1.optionalChaining(() => userData.location.city),
                state: lib_1.optionalChaining(() => userData.location.state),
                zip: lib_1.optionalChaining(() => userData.location.zip),
              },
            },
            context,
            '{id}',
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
            paymentAccount: context.payments.query.paymentAccount(
              {
                where: {
                  id: paymentAccount.id,
                },
              },
              context,
              `{
                id
                type
                status
                stripePaymentInformation {
                  id
                  customerId
                  source {
                      id
                      last4
                      brand
                      funding
                  }
                }
              }
            `,
            ),
          }
        } catch (e) {
          // console.log(e);
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            paymentAccount: null,
          }
        }
      })
    },
  },
})
