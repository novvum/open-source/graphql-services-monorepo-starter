'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const faker = require('faker')
exports.default = {
  User: () => ({
    profilePhotoPath: faker.image.cats(100, 100),
  }),
}
