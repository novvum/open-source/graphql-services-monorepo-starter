'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
    extend type DriverLegalInfo {
        licensePhoto: File
    }
    extend type User {
        profilePhoto: File
    }
`
exports.resolvers = mergeInfo => ({
  DriverLegalInfo: {
    licensePhoto: {
      fragment: `fragment LicensePhotoFragment on DriverLegalInfo { licensePhotoId }`,
      resolve(parent, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.licensePhotoId
          if (!id) {
            return null
          }
          return yield context.files.query.file(
            {
              where: {
                id: id,
              },
            },
            context,
            info,
          )
        })
      },
    },
  },
  User: {
    profilePhoto: {
      fragment: `fragment ProfilePhotoFragment on User { profilePhotoId }`,
      resolve(parent, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.profilePhotoId
          if (!id) {
            return null
          }
          return yield context.files.query.file(
            {
              where: {
                id: id,
              },
            },
            context,
            info,
          )
        })
      },
    },
  },
})
