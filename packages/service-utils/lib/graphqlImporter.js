'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const graphql_import_1 = require('graphql-import')
const path = require('path')
exports.importGraphqlFile = (baseDir, relativePath) => {
  return graphql_import_1.importSchema(path.join(baseDir, relativePath))
}
//# sourceMappingURL=graphqlImporter.js.map
