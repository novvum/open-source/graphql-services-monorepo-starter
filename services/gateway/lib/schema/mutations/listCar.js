'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
exports.linkTypeDefs = `
    input ListCarInput {
        city: String!
        state: String!
        street: String!
        zip: String!
        pickupLat: Float!
        pickupLng: Float!
        description: String
        vin: String!
        makeId: String!
        modelId: String!
        yearId: String!
        mileage: Int!
        maxDailyMiles: Int!
        dailyPriceInCents: Int!
        weeklyPriceInCents: Int!
        monthlyPriceInCents: Int!
        licensePlate: String!
        registrationExpirationAt: DateTime!
        photos: [Upload!]!
        registrationFile: Upload!
        uberLyftInspectionFile: Upload!
        personalInsuranceFile: Upload
    }
    type ListCarPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        car: Car
    }
    extend type Mutation {
        listCar(data: ListCarInput!): ListCarPayload
    }
    type ValidateVin implements MutationResponse {
        code: String
        success: Boolean
        message: String
    }
    extend type Query {
        validateVin(vin: String!): ValidateVin
    }
`
exports.resolvers = mergeInfo => ({
  Query: {
    validateVin(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        const cars = yield context.cars.query.carsConnection(
          {
            where: {
              vin: args.vin,
            },
          },
          context,
          '{ aggregate { count } }',
        )
        if (cars.aggregate.count > 0) {
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: 'This vin already taken.',
          }
        }
        return {
          code: lib_1.ResponseCodes.OK,
          success: true,
        }
      })
    },
  },
  Mutation: {
    listCar(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          validateRequest(context)
          const car = yield listCar(args.data, context)
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            car: car,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      })
    },
  },
})
function listCar(data, context) {
  return __awaiter(this, void 0, void 0, function*() {
    yield verifyCarData(context, data)
    const input = Object.assign({}, data, yield getMakeModelYear(data, context))
    const [
      uberLyftInspectionFile,
      registrationFile,
      personalInsuranceFile,
      ...photos
    ] = yield uploadCarDocuments(input, context)
    const car = yield submitCar(
      {
        registrationFile,
        uberLyftInspectionFile,
        personalInsuranceFile,
        input,
        photos,
      },
      context,
    )
    return car
  })
}
function verifyCarData(context, data) {
  return __awaiter(this, void 0, void 0, function*() {
    const existingCars = yield context.cars.query.cars(
      {
        where: {
          vin: data.vin,
        },
      },
      context,
      '{ id }',
    )
    if (existingCars.length > 0) {
      throw lib_1.makeResponseError({
        message:
          'A car with this vin already exists. Please change the vin and submit again.',
        code: lib_1.ResponseCodes.BAD_REQUEST,
      })
    }
  })
}
function validateRequest(context) {
  if (!context.currentUser || !context.currentUser.owner) {
    throw lib_1.makeResponseError({
      code: lib_1.ResponseCodes.UNAUTHORIZED,
      message: 'Please login to list a car.',
    })
  }
}
function submitCar(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const {
      registrationFile,
      uberLyftInspectionFile,
      personalInsuranceFile,
      input,
      photos,
    } = args
    const documents = {
      create: [
        {
          type: 'REGISTRATION',
          fileId: registrationFile.id,
        },
        {
          type: 'UBER_LYFT_INSPECTION',
          fileId: uberLyftInspectionFile.id,
        },
      ],
    }
    if (personalInsuranceFile) {
      documents.create = [
        ...documents.create,
        {
          type: 'PERSONAL_INSURANCE',
          fileId: personalInsuranceFile.id,
        },
      ]
    }
    const car = yield context.cars.mutation.createCar(
      {
        data: {
          ownerId: context.currentUser.owner.id,
          verification: 'PENDING_VERIFICATION',
          status: 'REMOVED',
          location: {
            create: {
              city: input.city,
              state: input.state,
              street: input.street,
              formattedAddress: `${input.street} ${input.city}, ${
                input.state
              } ${input.zip}`,
              zip: input.zip,
              lat: input.pickupLat,
              lng: input.pickupLng,
            },
          },
          description: input.description,
          vin: input.vin,
          make: input.make,
          makeId: input.makeId,
          model: input.model,
          modelId: input.modelId,
          year: input.year,
          yearId: input.yearId,
          mileage: input.mileage,
          maxDailyMiles: input.maxDailyMiles,
          dailyPriceInCents: input.dailyPriceInCents,
          weeklyPriceInCents: input.weeklyPriceInCents,
          monthlyPriceInCents: input.monthlyPriceInCents,
          licensePlate: input.licensePlate,
          registrationExpirationAt: input.registrationExpirationAt,
          documents: documents,
          photos: {
            create: photos.map((photo, index) => ({
              fileId: photo.id,
              isPrimary: index === 0,
            })),
          },
        },
      },
      context,
    )
    return car
  })
}
function uploadCarDocuments(input, context) {
  return __awaiter(this, void 0, void 0, function*() {
    return yield Promise.all([
      context.files.mutation.uploadFile(
        {
          data: {
            file: input.uberLyftInspectionFile,
            type: 'CAR_DOCUMENT',
          },
        },
        context,
        '{ id }',
      ),
      context.files.mutation.uploadFile(
        {
          data: {
            file: input.registrationFile,
            type: 'CAR_DOCUMENT',
          },
        },
        context,
        '{ id }',
      ),
      input.personalInsuranceFile
        ? context.files.mutation.uploadFile(
            {
              data: {
                file: input.personalInsuranceFile,
                type: 'CAR_DOCUMENT',
              },
            },
            context,
            '{ id }',
          )
        : Promise.resolve(null),
      ...input.photos.map((photo, index) =>
        __awaiter(this, void 0, void 0, function*() {
          return context.files.mutation.uploadFile(
            {
              data: {
                file: photo,
                type: 'CAR_PHOTO',
              },
            },
            context,
            '{ id }',
          )
        }),
      ),
    ])
  })
}
function getMakeModelYear(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const res = (yield context.legacyApi.get(
      `/db/vehicles/${args.makeId}/${args.modelId}/${args.yearId}`,
    )).data
    return {
      makeId: res.mark_id,
      make: res.mark,
      modelId: res.model_id,
      model: res.model,
      yearId: res.id,
      year: res.year,
    }
  })
}
exports.getMakeModelYear = getMakeModelYear
function getMakeModelYearFromStr(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const makes = (yield context.legacyApi.get(`/db/vehicles`)).data
    const foundMake = makes.find(
      make => make.mark.toUpperCase() === args.make.toUpperCase(),
    )
    if (!foundMake) {
      return {}
    }
    const models = (yield context.legacyApi.get(`/db/vehicles/${foundMake.id}`))
      .data
    const foundModel = models.find(
      model => model.model.toUpperCase() === args.model.toUpperCase(),
    )
    if (!foundModel) {
      return {
        makeId: foundMake.id,
        make: foundMake.mark,
      }
    }
    const years = (yield context.legacyApi.get(
      `/db/vehicles/${foundMake.id}/${foundModel.id}`,
    )).data
    const foundYear = models.find(year => year.year === args.year)
    if (!foundYear) {
      return {
        makeId: foundMake.id,
        make: foundMake.mark,
        modelId: foundModel.id,
        model: foundModel.model,
      }
    }
    const res = (yield context.legacyApi.get(
      `/db/vehicles/${foundMake.id}/${foundModel.id}/${foundYear.id}`,
    )).data
    return {
      makeId: res.mark_id,
      make: res.mark,
      modelId: res.model_id,
      model: res.model,
      yearId: res.id,
      year: res.year,
    }
  })
}
exports.getMakeModelYearFromStr = getMakeModelYearFromStr
