import { MergeSchemaHelper } from 'service-utils/lib'
import { context } from '../getContext'
import * as sampleStitch from './sampleStitch'

import mutations from './mutations'

const schemas = [context.prismaSample.schema]

const mergedSchema: MergeSchemaHelper = new MergeSchemaHelper()
schemas.forEach(schema => {
  mergedSchema.addSchema(schema)
})

mergedSchema.addSchema(sampleStitch.linkTypeDefs)
mergedSchema.addResolver(sampleStitch.resolvers)

mergedSchema.addSchemas(mutations.schemas)
mergedSchema.addResolvers(mutations.resolvers)

export default mergedSchema
