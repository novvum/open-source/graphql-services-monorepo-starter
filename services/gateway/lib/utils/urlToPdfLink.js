'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
function urlToPdfLink(url, additionalParams = {}) {
  const queryString = require('querystring')
  const query = queryString.stringify(Object.assign({ url }, additionalParams))
  return `https://url-to-pdf-api.herokuapp.com/api/render?${query}`
}
exports.urlToPdfLink = urlToPdfLink
