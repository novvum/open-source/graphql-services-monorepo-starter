'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
exports.linkTypeDefs = `
    input StartBackgroundCheckInput {
        licenseNumber: String!
        licenseState: String!
        ssn: String!
        dob: DateTime!
        couponCode: String
    }
    type StartBackgroundCheckPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        backgroundCheck: BackgroundCheck
    }
    extend type Mutation {
        startBackgroundCheck(data: StartBackgroundCheckInput!): StartBackgroundCheckPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    startBackgroundCheck(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const input = args.data
          const user = context.currentUser
          const backgroundCheck = yield startBackgroundCheck(
            { user, input },
            context,
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
            backgroundCheck,
          }
        } catch (e) {
          return {
            code: lib_1.ResponseCodes.REQUEST_FAILED,
            success: false,
            message: e.message,
            backgroundCheck: null,
          }
        }
      })
    },
  },
})
function startBackgroundCheck(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { user, input } = args
    const bgcPayment = yield chargeBgc({ user, input }, context)
    const backgroundCheck = yield submitBgc(input, context)
    return backgroundCheck
  })
}
function submitBgc(input, context) {
  return __awaiter(this, void 0, void 0, function*() {
    return yield context.users.mutation.startCurrentUserBgc(
      {
        data: {
          driverLicenseNumber: input.licenseNumber,
          driverLicenseState: input.licenseState,
          dob: input.dob,
          ssn: input.ssn,
        },
      },
      context,
    )
  })
}
function chargeBgc(args, context) {
  return __awaiter(this, void 0, void 0, function*() {
    const { user, input } = args
    return yield context.payments.mutation.chargeBGC(
      {
        data: {
          userId: user.id,
          couponCode: input.couponCode,
          description: `Driver #${user.driver.id} pays for MVR check.`,
        },
      },
      context,
      '{id}',
    )
  })
}
