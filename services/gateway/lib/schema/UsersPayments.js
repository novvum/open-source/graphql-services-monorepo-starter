'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
exports.linkTypeDefs = `
  extend type User {
    paymentAccounts(where: PaymentAccountWhereInput, orderBy: PaymentAccountOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [PaymentAccount]!
    payoutAccounts(where: PayoutAccountWhereInput, orderBy: PayoutAccountOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [PayoutAccount]!
  }
  extend type PaymentAccount {
      user: User!
  }
  extend type PayoutAccount {
    user: User!
}
`
exports.resolvers = mergeInfo => ({
  PaymentAccount: {
    user: {
      fragment: `fragment UserFragment on PaymentAccount { userId }`,
      resolve(parent, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.userId
          return mergeInfo.delegate(
            'query',
            'user',
            {
              where: {
                id,
              },
            },
            context,
            info,
          )
        })
      },
    },
  },
  PayoutAccount: {
    user: {
      fragment: `fragment UserFragment on PayoutAccount { userId }`,
      resolve(parent, _, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.userId
          return mergeInfo.delegate(
            'query',
            'user',
            {
              where: {
                id,
              },
            },
            context,
            info,
          )
        })
      },
    },
  },
  User: {
    paymentAccounts: {
      fragment: `fragment UserFragment on User { id }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.id
          // TODO: get list of owner's car id's for OR condition below
          return mergeInfo.delegate(
            'query',
            'paymentAccounts',
            Object.assign({}, args, {
              where: {
                AND: [{ userId: id }, args.where || {}],
              },
            }),
            context,
            info,
          )
        })
      },
    },
    payoutAccounts: {
      fragment: `fragment UserFragment on User { id }`,
      resolve(parent, args, context, info) {
        return __awaiter(this, void 0, void 0, function*() {
          const id = parent.id
          // TODO: get list of owner's car id's for OR condition below
          return mergeInfo.delegate(
            'query',
            'payoutAccounts',
            Object.assign({}, args, {
              where: {
                AND: [{ userId: id }, args.where || {}],
              },
            }),
            context,
            info,
          )
        })
      },
    },
  },
})
