'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
exports.linkTypeDefs = `
    input ApproveRejectDriverBgcInput {
        driverId: ID!
    }
    type ApproveRejectDriverBgcPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        driver: Driver
    }
    extend type Mutation {
        adminApproveDriverBgc(data: ApproveRejectDriverBgcInput!): ApproveRejectDriverBgcPayload
        adminRejectDriverBgc(data: ApproveRejectDriverBgcInput!): ApproveRejectDriverBgcPayload
        adminUnresponsiveDriverBgc(data: ApproveRejectDriverBgcInput!): ApproveRejectDriverBgcPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    adminApproveDriverBgc(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const updatedDriver = yield context.users.mutation.updateDriver(
            {
              where: {
                id: args.data.driverId,
              },
              data: {
                backgroundCheck: {
                  update: {
                    verificationStatus: 'VERIFIED',
                  },
                },
              },
            },
            context,
            '{id backgroundCheck {id verificationStatus}}',
          )
          yield context.rentals.mutation.updateManyRentals(
            {
              where: {
                driverId: updatedDriver.id,
                status: 'APPLIED_NOT_VERIFIED',
              },
              data: {
                status: 'APPLIED',
              },
            },
            context,
            '{count}',
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            driver: updatedDriver,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      })
    },
    adminRejectDriverBgc(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const updatedDriver = yield context.users.mutation.updateDriver(
            {
              where: {
                id: args.data.driverId,
              },
              data: {
                backgroundCheck: {
                  update: {
                    verificationStatus: 'REJECTED',
                  },
                },
              },
            },
            context,
            '{id backgroundCheck {id verificationStatus}}',
          )
          yield context.rentals.mutation.updateManyRentals(
            {
              where: {
                driverId: updatedDriver.id,
                status: 'APPLIED_NOT_VERIFIED',
              },
              data: {
                status: 'APPLICATION_AUTO_REJECTED',
              },
            },
            context,
            '{count}',
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
            driver: updatedDriver,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      })
    },
    adminUnresponsiveDriverBgc(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const updatedDriver = yield context.users.mutation.updateDriver(
            {
              where: {
                id: args.data.driverId,
              },
              data: {
                backgroundCheck: {
                  update: {
                    verificationStatus: 'UNRESPONSIVE',
                  },
                },
              },
            },
            context,
            '{id backgroundCheck {id verificationStatus}}',
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            message: null,
            driver: updatedDriver,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
          }
        }
      })
    },
  },
})
