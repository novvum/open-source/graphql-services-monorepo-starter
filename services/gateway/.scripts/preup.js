const path = require('path')
require('dotenv').config({ path: path.resolve(__dirname, '../../../.env') })
const fs = require('fs')
const upJson = require('../up.json')

const { environment } = upJson

Object.keys(process.env)
  .filter(key => key.startsWith('GRAPHQL_'))
  .forEach(key => {
    if (process.env[key]) {
      environment[key] = process.env[key]
      if (process.env[`${key}_PROD`]) {
        environment[`${key}_PROD`] = process.env[`${key}_PROD`]
      }
    }
  })

environment['NODE_ENV'] = 'production'
upJson.environment = environment
const result = JSON.stringify(upJson, null, 2)

fs.writeFileSync('./up.json', `${result}\n`)
console.log('finished preup')
