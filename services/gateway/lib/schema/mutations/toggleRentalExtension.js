'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const lib_1 = require('service-utils/lib')
const approveRejectBooking_1 = require('./approveRejectBooking')
exports.linkTypeDefs = `
    input ToggleRentalExtensionInput {
        rentalId: ID!
    }
    type ToggleRentalExtensionPayload implements MutationResponse {
        code: String
        success: Boolean
        message: String
        rental: Rental
    }
    extend type Mutation {
        toggleRentalExtension(data: ToggleRentalExtensionInput!): ToggleRentalExtensionPayload
        adminToggleRentalExtension(data: ToggleRentalExtensionInput!): ToggleRentalExtensionPayload
    }
`
exports.resolvers = mergeInfo => ({
  Mutation: {
    toggleRentalExtension(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const rental = yield context.rentals.query.rental(
            {
              where: {
                id: args.data.rentalId,
              },
            },
            context,
            '{ id driverId disallowExtensions carId rentalPeriods { id } }',
          )
          if (
            !(yield approveRejectBooking_1.isCurrentUserCarOwner(
              rental.carId,
              context,
            ))
          ) {
            return lib_1.NOT_AUTHORIZED_RESPONSE
          }
          const updatedRental = yield context.rentals.mutation.updateRental(
            {
              where: {
                id: rental.id,
              },
              data: {
                disallowExtensions: !rental.disallowExtensions,
              },
            },
            context,
            '{ id disallowExtensions }',
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            rental: updatedRental,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            rental: null,
          }
        }
      })
    },
    adminToggleRentalExtension(_, args, context, info) {
      return __awaiter(this, void 0, void 0, function*() {
        try {
          const rental = yield context.rentals.query.rental(
            {
              where: {
                id: args.data.rentalId,
              },
            },
            context,
            '{ id driverId disallowExtensions carId rentalPeriods { id } }',
          )
          const updatedRental = yield context.rentals.mutation.updateRental(
            {
              where: {
                id: rental.id,
              },
              data: {
                disallowExtensions: !rental.disallowExtensions,
              },
            },
            context,
            '{ id disallowExtensions }',
          )
          return {
            code: lib_1.ResponseCodes.OK,
            success: true,
            rental: updatedRental,
          }
        } catch (e) {
          console.error(e)
          return {
            code: lib_1.ResponseCodes.BAD_REQUEST,
            success: false,
            message: e.message,
            rental: null,
          }
        }
      })
    },
  },
})
