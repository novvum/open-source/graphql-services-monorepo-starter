'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const moment = require('moment')
function getDate(date, func) {
  let parsedDate = new Date(date)
  const validDate = parsedDate instanceof Date && !isNaN(parsedDate.valueOf())
  if (!date || date === '0000-00-00 00:00:00' || !validDate) {
    parsedDate = new Date(0)
  }
  const formattedDate = moment(new Date(parsedDate)).format(
    'YYYY-MM-DDTHH:mm:ss.SSS[Z]',
  )
  return formattedDate
}
exports.getDate = getDate
class AuthError extends Error {
  constructor() {
    super('Not authorized')
  }
}
exports.AuthError = AuthError
exports.loadEnv = env => {
  if (process.env.UP_STAGE === 'production') {
    if (process.env[`${env}_PROD`]) {
      return process.env[`${env}_PROD`]
    }
  }
  return process.env[env]
}
function optionalChaining(func) {
  try {
    return func()
  } catch (e) {
    return undefined
  }
}
exports.optionalChaining = optionalChaining
//# sourceMappingURL=utils.js.map
