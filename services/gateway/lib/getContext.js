'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done
          ? resolve(result.value)
          : new P(function(resolve) {
              resolve(result.value)
            }).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
Object.defineProperty(exports, '__esModule', { value: true })
const config_1 = require('./config')
const lib_1 = require('service-utils/lib')
const lib_2 = require('@services/users/lib')
const lib_3 = require('@services/rentals/lib')
const lib_4 = require('@services/cars/lib')
const lib_5 = require('@services/ratings/lib')
const lib_6 = require('@services/payments/lib')
const lib_7 = require('@services/files/lib')
const lib_8 = require('@services/admin/lib')
const lib_9 = require('@services/insurance/lib')
const lib_10 = require('@services/notifications/lib')
const event_gateway_sdk_1 = require('@serverless/event-gateway-sdk')
const context = {}
exports.context = context
const DEBUG = false // process.env.GRAPHQL_STAGE === "dev";
const usersBinding = new lib_2.UsersBinding({
  debug: DEBUG,
  prismaEndpoint: config_1.default.USERS_PRISMA_ENDPOINT,
  prismaSecret: config_1.default.GRAPHQL_PRISMA_SECRET,
})
context.users = usersBinding
const rentalsBinding = new lib_3.RentalsBinding({
  debug: DEBUG,
  prismaEndpoint: config_1.default.RENTALS_PRISMA_ENDPOINT,
  prismaSecret: config_1.default.GRAPHQL_PRISMA_SECRET,
})
context.rentals = rentalsBinding
const ratingsBinding = new lib_5.RatingsBinding({
  debug: DEBUG,
  prismaEndpoint: config_1.default.RATINGS_PRISMA_ENDPOINT,
  prismaSecret: config_1.default.GRAPHQL_PRISMA_SECRET,
})
context.ratings = ratingsBinding
const carsBinding = new lib_4.CarsBinding({
  debug: DEBUG,
  prismaEndpoint: config_1.default.CARS_PRISMA_ENDPOINT,
  prismaSecret: config_1.default.GRAPHQL_PRISMA_SECRET,
})
context.cars = carsBinding
const adminBinding = new lib_8.AdminBinding({
  debug: DEBUG,
  prismaEndpoint: config_1.default.ADMIN_PRISMA_ENDPOINT,
  prismaSecret: config_1.default.GRAPHQL_PRISMA_SECRET,
})
context.admin = adminBinding
const paymentsBinding = new lib_6.PaymentsBinding({
  debug: DEBUG,
  prismaEndpoint: config_1.default.PAYMENTS_PRISMA_ENDPOINT,
  prismaSecret: config_1.default.GRAPHQL_PRISMA_SECRET,
})
context.payments = paymentsBinding
const filesBinding = new lib_7.FilesBinding({
  debug: DEBUG,
  prismaEndpoint: config_1.default.FILES_PRISMA_ENDPOINT,
  prismaSecret: config_1.default.GRAPHQL_PRISMA_SECRET,
})
context.files = filesBinding
const insuranceBinding = new lib_9.InsuranceBinding({
  debug: DEBUG,
  prismaEndpoint: config_1.default.INSURANCE_PRISMA_ENDPOINT,
  prismaSecret: config_1.default.GRAPHQL_PRISMA_SECRET,
})
context.insurance = insuranceBinding
const notificationsBinding = new lib_10.NotificationsBinding({
  token: null,
})
context.notifications = notificationsBinding
context.legacyApi = new lib_1.LegacyApi({
  baseUri: config_1.default.legacyApiUrl,
  token: '',
})
context.eventGateway = new event_gateway_sdk_1.default({
  url: config_1.default.eventGatewayUrl,
})
const getContext = args =>
  __awaiter(this, void 0, void 0, function*() {
    context.request = args.request
    context.currentUser = yield context.users.auth.getCurrentUser(context)
    if (
      lib_1.optionalChaining(() => context.currentUser.roles.includes('admin'))
    ) {
      context.currentUser.admin = yield getCurrentUserAdmin(context)
    }
    context.eventGateway.emit({
      event: 'user.created',
      data: { userId: 1234 },
    })
    return context
  })
exports.getContext = getContext
function getCurrentUserAdmin(context) {
  return __awaiter(this, void 0, void 0, function*() {
    const userDetails = {
      userId: context.currentUser.id,
    }
    const existingAdmin = yield context.admin.query.admin(
      {
        where: userDetails,
      },
      context,
      '{ id }',
    )
    if (!existingAdmin) {
      return yield context.admin.mutation.upsertAdmin(
        {
          where: userDetails,
          create: userDetails,
          update: userDetails,
        },
        context,
        '{ id }',
      )
    }
    return existingAdmin
  })
}
